#include <uart.h>
#include <mmio.h>
#include <memoryMap.h>

// Driver for ARM PL011

#define UART0_WRITE(offset, val)  mmioWrite8(UART0 + offset, val)
#define UART0_READ(offset)        mmioRead8(UART0 + offset)

#define PL011_DATA        0x0
#define PL011_CTRL        0x30
#define PL011_CTRL_EN     (1 << 0)
#define PL011_CTRL_TX_EN  (1 << 0)

void uartPrint(const Char8 *str) {
  while(*str) {
    UART0_WRITE(PL011_DATA, *str++);
  }
}

void uartWrite(const Char8 *str, Uintn length) {
  for (Uintn i = 0; i < length; i++) {
    UART0_WRITE(PL011_DATA, str[i]);
  }
}

void uartInit() {
  UART0_WRITE(
    PL011_CTRL,
    UART0_READ(PL011_CTRL) | PL011_CTRL_EN | PL011_CTRL_TX_EN
    );
}
