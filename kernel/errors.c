#include <uart.h>
#include <errors.h>
#include <boot.h>
#include <scheduler.h>
#include <memoryMap.h>
#include <armSysCtrlRegisters.h>

 // Functions called from the exception vectors

void dead() __attribute__((noreturn));
void dead() {
  while (1);
}

SignalHandler fSignalHandler = NULL;

void setSignalHandler(SignalHandler handler) {
  fSignalHandler = handler;
}

// Print a human-readable interpretatoin of the Data Fault Status Register
static void printDfsr(Uint32 dfsr) {
  printk("Fault on %s.\n", (dfsr & DFSR_READ_NOT_WRITE) ? "read" : "write");

  Uintn faultStatus = DFSR_GET_FAULT_STATUS(dfsr);

  printk("Fault type 0x%x", faultStatus);
  switch (faultStatus) {
    case 0x1: printk(": alignment fault"); break;
    case 0xd: printk(": permission fault (first level)"); break;
    case 0xf: printk(": permission fault (second level)"); break;
    default: break;
  }

  printk("\n");
}

static void printError(Char8 *errorName, SignalType signalType) {
  printk("Unhandled %s. Context: ", errorName);
  PRINT_CONTEXT(*gCurrentContextBuffer);
  switch (signalType) {
    case prefetchAbortSignal:
      printk("IFSR: 0x%x\n", (Uintn) getInstructionFaultStatus());
      break;
    case dataAbortSignal:
      printk("Data fault at 0x%x\n", (Uintn) getDataFaultAddress());
      printDfsr(getDataFaultStatus());
      break;
    default: break;
  }
}

#define DEFINE_ERROR_FUNC(funcName, errorName, signalType) \
  void funcName() { \
    if (fSignalHandler != NULL) { \
      deferSignalHandlerCall(fSignalHandler, signalType); \
    } else { \
      printError(errorName, signalType); \
      dead(); \
    } \
  }

DEFINE_ERROR_FUNC(invalidSyscall, "invalid system call", invalidSyscallSignal)
DEFINE_ERROR_FUNC(undefinedInstruction, "undefined instruction abort",
    undefinedInstructionSignal)
DEFINE_ERROR_FUNC(prefetchAbort, "prefetch abort",
  prefetchAbortSignal)
DEFINE_ERROR_FUNC(dataAbort, "data abort", dataAbortSignal)

void fastInterrupt() {
  uartPrint("fastInterrupt\n");
  dead();
}
