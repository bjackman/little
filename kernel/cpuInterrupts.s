@ Code to enable and disable interrupts at the CPU level

/* Enable IRQs in the CPU */
.global cpuEnableInterrupts
cpuEnableInterrupts:
  mrs r0, CPSR
  bic r0, r0, #(1 << 7)
  msr CPSR, r0
  mov pc, lr

/* Disable IRQs in the CPU */
.global cpuDisableInterrupts
cpuDisableInterrupts:
  mrs r0, CPSR
  orr r0, r0, #(1 << 7)
  msr CPSR, r0
  mov pc, lr
