// Functions for setting page tables

// Terminology: 
//  "Translation table entry" (TTE) is the generic term.
//  "Page table entry" (PTE) refers specifically to entries in the L2 translation 
//    table.
// See Cortex-A Programmer's guide for explaination of field names

#include <pageTable.h>
#include <memoryUtils.h>
#include <memoryMap.h>
#include <memoryType.h>
#include <boot.h>

// Master Translation Table has an entry for each 1M chunk in the 4Gb address
// space, so there are 4K entries.
#define L1_TTE_NUM_ENTRIES SIZE_4K

//
// Master translation table entry field values
//

#define L1_TTE_TYPE_FAULT     0x0
#define L1_TTE_TYPE_PTE       0x1
#define L1_TTE_TYPE_SECTION   0x2

#define L1_TTE_PRIVILEGED_ACCESS_ONLY  (0x1 << 10) // AP = 01, APX = 0
#define L1_TTE_FULL_ACCESS             (0x3 << 10) // AP = 11, APX = 0

// B = 1, C = 1, TEX = 001
#define L1_TTE_NORMAL_CACHE_WRITE_BACK (1 << 2 | 1 << 3 | 1 << 12)
// B = 1, C = 0, TEX = 000
#define L1_TTE_SHAREABLE_DEVICE (1 << 2 | 1 << 3 | 1 << 12)

#define L1_TTE_USER_DATA     (L1_TTE_FULL_ACCESS | \
                              L1_TTE_NORMAL_CACHE_WRITE_BACK | \
                              L1_TTE_TYPE_SECTION)

#define L1_TTE_KERNEL_MEMORY (L1_TTE_PRIVILEGED_ACCESS_ONLY | \
                              L1_TTE_NORMAL_CACHE_WRITE_BACK | \
                              L1_TTE_TYPE_SECTION)

#define L1_TTE_DEVICE        (L1_TTE_PRIVILEGED_ACCESS_ONLY | \
                              L1_TTE_SHAREABLE_DEVICE | \
                              L1_TTE_TYPE_SECTION)

#define L1_TTE_FAULT          L1_TTE_TYPE_FAULT
//
// L2 Page table entry field values
//

// Page table entry type
#define L2_TTE_TYPE_SMALL 0x2

// Access mode
#define L2_TTE_PRIVILEGED_ACCESS_ONLY  (0x1 << 4) // AP = 01, APX = 0
#define L2_TTE_FULL_ACCESS             (0x3 << 4) // AP = 11, APX = 0

// Memory type and cache policy
#define L2_TTE_NORMAL_CACHE_WRITE_BACK (1 << 2 | 1 << 3) // B = 1, C = 1


// Point a L1 table entry at a L2 table entry
static void insertPageTable(MasterTranslationTableEntry *masterTableEntry,
                            PageTableEntry              *pageTable) {
  // L2 table must be aligned on 1K
  ASSERT(ALIGNED_ON(pageTable, SIZE_1K));

  // Leave all bits zero except PTE address and entry type.
  *masterTableEntry = (Uintn) pageTable | L1_TTE_TYPE_PTE;
}

// Create an L1 Section entry mapping physicalAddress at virtualAddress
static void mapSection(UintPtr virtualAddress, UintPtr physicalAddress) {
  ASSERT(ALIGNED_ON(virtualAddress, SIZE_1M));
  ASSERT(ALIGNED_ON(physicalAddress, SIZE_1M));

  Uintn sectionIndex = virtualAddress >> 20;

  gMasterTranslationTable[sectionIndex] = physicalAddress |
                                          L1_TTE_PRIVILEGED_ACCESS_ONLY |
                                          L1_TTE_NORMAL_CACHE_WRITE_BACK |
                                          L1_TTE_TYPE_SECTION;

}

/*
  Configure the memory protection for some pages
*/
void setMemoryType(void *page, Uint32 size, MemoryType type) {
  ASSERT(type < maxMemoryType);
  ASSERT(ALIGNED_ON(page, PAGE_SIZE));

  Uint32 memoryFlags;

  switch (type) {
    case userData:
      memoryFlags = L1_TTE_USER_DATA;
      break;
    case kernelData:
      memoryFlags = L1_TTE_KERNEL_MEMORY;
      break;
    default:
      // We dun goof'd if we got here (especially with the assertions above)
      ASSERT(FALSE);
      return;
  }

  UintPtr _page = (UintPtr) page;
  for (UintPtr address = _page; address < _page + size; address += SIZE_1M) {
    Uint32 i = address / SIZE_1M;
    gMasterTranslationTable[i] = address | memoryFlags;
  }
}

/*
  Set up the page mapping.
  Currently we use an identity mapping. Devices are marked as such,
  the GPMC (general purpose memory controller) is unmapped, kernel memory
  is protected, and user memory is mapped as normal accessible memory.
*/
void setupPageMapping () {
  printk("Master translation table at 0x%p\n",
         (void *) gMasterTranslationTable);

  // L1 table must be aligned on 16K
  ASSERT(ALIGNED_ON(gMasterTranslationTable, SIZE_16K));

  //
  // Set up an identity mapping.
  //

  // Set up the GPMC area. In proper use this would be mapped as privileged only
  // For now, since we're identity-mapped and we don't use the GPMC, we'll just
  // map it as no access - that means we'll get aborts when we dereference NULL.
  UintPtr address;
  for (address = 0; address < AM37X_GPMC_SIZE; address += SIZE_1M) {
    Uintn i = address / SIZE_1M;
    gMasterTranslationTable[i] = L1_TTE_FAULT;
  }

  // Set up the peripherals area - privileged only, device memory
  for ( ; address < AM37X_PERIPH_BASE + AM37X_PERIPH_SIZE; address += SIZE_1M) {
    Uintn i = address / SIZE_1M;
    gMasterTranslationTable[i] = address | L1_TTE_DEVICE;
  }

  // Set up the kernel RAM
  ASSERT(ALIGNED_ON(&gUsrAreaStart, SIZE_1M));
  for ( ; address < (UintPtr) &gUsrAreaStart; address += SIZE_1M) {
    Uintn i = address / SIZE_1M;
    gMasterTranslationTable[i] = address | L1_TTE_KERNEL_MEMORY;
  }

  // Set up the user RAM
  for ( ; address < AM37X_DRAM_BASE + AM37X_DRAM_SIZE; address += SIZE_1M) {
    Uintn i = address / SIZE_1M;
    gMasterTranslationTable[i] = address | L1_TTE_USER_DATA;
  }

  // Set up the unused stuff
  // We're a bit naughty here: the exit condition is address overflowing.
  // Unsigned overflow is defined by C, so this _shouldn't_ be an issue.
  for ( ; address != 0; address += SIZE_1M) {
    Uintn i = address / SIZE_1M;
    gMasterTranslationTable[i] = L1_TTE_FAULT;
  }
}
