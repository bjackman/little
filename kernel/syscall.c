#include <syscalls.h>
#include <uart.h>
#include <errors.h>
#include <tick.h>
#include <scheduler.h>
#include <fifo.h>
#include <read.h>

/*
  As a moderately evil hack, we handle syscalls with a table of functions,
  syscallHandlers[].

  When we initialise it we cast all the functions to void (*)() - that is a
  pointer to a function that takes any number of arguments and returns void.

  In order to simplify the process of calling them, these
  functions must take a Uint32 as their first argument. This will be the syscall
  number passed from userspace in r0, and may be ignored.

  Since these functions are called from assembly, we believe that this hackery
  is fairly safe.
*/

/*
  Since we added SYS_wait, it's possible for the running thread to
  change during handling a syscall. The simplest thing to do in
  response to this was to save an entire context when an SVC is taken,
  and then to use restoreUserContext to return from it. We therefore
  use this macro to set the r0 that will be restored when we return
  from the syscall.

  TODO: Create some fancy macro for doing this properly. Right now
  let's just do it manually
*/

Uint32 gMaxSyscallNumber = numSyscalls - 1;

void handleWrite(Uint32 ignored, Uintn fd, Char8 *str, Uintn len) {
  if (fd < 3) {
    // Write to a standard steam. At the moment this is always the uart.
    uartWrite(str, len);
    gCurrentContextBuffer->r0 = len;
  } else {
    // No other files yet implemented
    printk("Got write for unimplemented file descriptor: %d\n", fd);
    gCurrentContextBuffer->r0 = -1;
  }
}

void handleReadChar(Uint32 ignored, Uintn fd) {
  readChar(fd);
}

void handleHandleSignal(Uint32 ignored, SignalHandler signalHandler) {
  setSignalHandler(signalHandler);
}

void handleGetTickCountMs(Uint32 ignored, Uint64 *result) {
  // Manually put the 64-bit value into return registers.

  Uint64 tickCount = getTickCountMs();
  gCurrentContextBuffer->r0 = (Uint32) tickCount;
  gCurrentContextBuffer->r1 = (Uint32) (tickCount >> 32);
}

void handleCreateThread(Uint32 ignored, void (*threadMain)(), void *entryArg) {
  gCurrentContextBuffer->r0 = addUserThread(threadMain, entryArg);
}

void handleWait(Uint32 ignored, Uint64 timeMs) {
  // waitCurrentThread could result in changing the running thread.
  // Remember the caller's context so we can set the return value.
  ArmContext *callerContext = gCurrentContextBuffer;
  StatusCode status = waitCurrentThread(timeMs);
  callerContext->r0 = status;
}

void handleExit(Uint32 ignored) {
  // exitCurrentThread will result in changing the running thread.
  // Remember the caller's context so we can set the return value.
  ArmContext *callerContext = gCurrentContextBuffer;
  StatusCode status = exitCurrentThread();
  callerContext->r0 = status;
};

// TODO: could probably directly insert handlers into here instead of
// having handle* functions that do nothing but call a single
// function..
#pragma pack(0)
void (*syscallHandlers[numSyscalls])() = {
  [SYS_write] =          handleWrite,
  [SYS_readChar] =       handleReadChar,
  [SYS_handleSignal] =   handleHandleSignal,
  [SYS_getTickCountMs] = handleGetTickCountMs,
  [SYS_createThread] =   handleCreateThread,
  [SYS_wait] =           handleWait,
  [SYS_exit] =           handleExit
};
#pragma pack()
