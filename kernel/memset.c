//TODO Write this in assembly? Might be able to make it faster with STM.
void *memset(void *_buffer, Uint8 val, Uintn length) {
  Uint8 *buffer = (Uint8 *) _buffer;

  for (Uintn i = 0; i < length; i++) {
    *(buffer++) = val;
  }
  return (void *) buffer;
}
