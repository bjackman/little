#include <mmio.h>

void mmioWrite8(Uintn addr, Uint8 val) {
  *(volatile char *) addr = val;
}

Uint8 mmioRead8(Uintn addr) {
  return *(volatile char *) addr;
}

void mmioWrite32(Uintn addr, Uint32 val) {
  *(volatile Uint32 *) addr = val;
}

Uint32 mmioRead32(Uintn addr) {
  return *(volatile Uint32 *) addr;
}
