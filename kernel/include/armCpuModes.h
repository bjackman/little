#ifndef _ARM_CPU_MODES_H__
#define _ARM_CPU_MODES_H__

#define MODE_SYSTEM     0x1f
#define MODE_IRQ        0x12
#define MODE_SVC        0x13
#define MODE_ABORT      0x17
#define MODE_UNDEFINED  0x1b

#endif
