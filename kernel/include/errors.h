#ifndef __ERRORS_H__
#define __ERRORS_H__

typedef enum {
  prefetchAbortSignal, // TODO remove "Signal" (added to avoid symbol conflict)
  dataAbortSignal,
  undefinedInstructionSignal,
  invalidSyscallSignal,
  maxAbortType
} SignalType;

// A handler the user has added via the SYS_handleSignal syscall
typedef void (*SignalHandler)(SignalType signalType, Uintn threadId);

void setSignalHandler(SignalHandler handler);

#endif
