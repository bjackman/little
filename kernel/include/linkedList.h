#ifndef __LINKED_LIST_H__
#define __LINKED_LIST_H__

/*
  Macros for linked lists.

  This is just like BSD's sys/queue.h, but with fewer features, worse clarity,
  and more bugs.

  Basically, the way it works is this: to make a type, T, into a list,
  you need to add a LIST_ENTRY(T) as one of its members, and create a
  head type LIST_HEAD(X, T). The LIST_ENTRY will have a name, which you
  will later pass to list manipulation macros as the "field"
  parameter. Then to instantiate a list of T, declare an X. This list
  head doesn't contain any data, it's just a sort of anchor for the
  list.

  Er.. hopefully that's at least somewhat clear. Have a look in
  scheduler.c for an example
*/

// Add a field of this type to enable you to put a struct in a list
// `next` will point to the next entry in the list.
// `prev` will point to the `next` field of the previous entry in the list.
#define LIST_ENTRY(type) struct { type *next; type **prev; }

// Unfortunately anonymous structs in C are arbitrarily useless, so we can't
// have nice macros that look like generic types. We have to name the type of a
// list head for each type we want lists of. This macro will do that for you.
#define LIST_HEAD(name, type)  typedef struct { type *first; } name

#define LIST_INIT(head) (head)->first = NULL

#define LIST_FIRST(head) (head)->first

#define LIST_NEXT(item, field) (item)->field.next

#define LIST_INSERT_HEAD(head, val, field) do { \
  (val)->field.next = (head)->first; \
  (val)->field.prev = &(head)->first; \
  if ((head)->first != NULL) \
    (head)->first->link.prev = &(val)->field.next; \
  (head)->first = (val); \
} while (0)

#define LIST_INSERT_BEFORE(after, val, field) do { \
  (val)->field.next = after; \
  (val)->field.prev = after->field.prev; \
  *(after)->field.prev = val; \
  (after)->field.prev = &(val)->field.next;                 \
} while (0)

#define LIST_REMOVE(item, field) do { \
  if ((item)->field.next != NULL) \
    (item)->field.next->field.prev = (item)->field.prev; \
  *(item)->field.prev = (item)->field.next; \
} while (0)

#define LIST_FOREACH_UNSAFE(head, type, item, field) \
  for ( type *item = (head)->first; (item) != NULL; (item) = (item)->field.next )

#endif
