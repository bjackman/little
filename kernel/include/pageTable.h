#ifndef __PAGE_TABLE_H__
#define __PAGE_TABLE_H__

#include <memoryType.h>

// Functions for setting page tables

typedef Uint32 MasterTranslationTableEntry;
typedef Uint32 PageTableEntry;

// Defined in linker script:
extern MasterTranslationTableEntry gMasterTranslationTable[];
extern PageTableEntry              gPageTable[];

void setupPageMapping();

/*
  Configure the memory protection for a page.
*/
void setMemoryType(void *page, Uint32 size, MemoryType type);

// An L2 translation table accounts for 1MB of memory, each entry accounting for
// 4KB.
#define L2_TRANSLATION_TABLE_SIZE 0x100

#endif
