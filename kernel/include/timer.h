#ifndef __TIMER_H__
#define __TIMER_H__

// Callback for timer interrupt. Arguments are:
//   - The time in milliseconds since it was last called
//   - The user context that was interrupted
typedef void (*TimerCallback)(Uintn timeSinceLastTick);

// Initialise the timer, enabling the tick interrupt. On each timer interrupt,
// call the callback.
void timerInit(void (*callback)());

#endif
