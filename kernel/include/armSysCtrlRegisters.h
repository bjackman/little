#ifndef __DATA_FAULT_ADDRESS_H__
#define __DATA_FAULT_ADDRESS_H__

/* 
  Functions for getting the ARM System Control registers.
  See ARMv7 A.R.M. section B4 for details
*/

// Read the DFAR
Uint32 getDataFaultAddress();

// See ARMv7 A-R-M issue C section B4.1.52 for DFSR format
#define DFSR_READ_NOT_WRITE (1 << 12)
#define DFSR_GET_FAULT_STATUS(dfsr) (((dfsr) & (1 << 10) >> 6) | ((dfsr) & 0xf))
// Read the DFSR
Uint32 getDataFaultStatus();

// Read the IFSR
Uint32 getInstructionFaultStatus();

#endif
