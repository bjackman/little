#ifndef __TICK_H__
#define __TICK_H__

void onTimerTick();

/* Return a count of millisecond ticks that comes from the system timer */
Uint64 getTickCountMs();

/*
  Add an item to the timer event queue. When the kernel millisecond
  counter exceeds triggerTime, call calllback, passing context as an
  argument.
*/
typedef void (*TimerEventCallback)(void *context);

StatusCode addTimerEvent(Uint64 triggerTime, TimerEventCallback callback,
                         void *context);

void timerEventInit();

#endif
