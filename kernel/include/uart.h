#ifndef __UART_H__
#define __UART_H__

#include <fifo.h>

void uartInit();

void uartPrint(const char *str);
void uartWrite(const char *str, Uintn length);

// Add an event to be triggered next time a character is received
// The callback will be called with the context argument.
// There is only one UART receive event and it triggers only once.
typedef void (*UartCallback)(void *context);
void setUartReceiveEvent(UartCallback callback, void *context);

#define UART_READ_BUFFER_SIZE 8
Fifo gUartReadFifo;

#endif
