#ifndef __DEBUG_H__
#define __DEBUG_H__

#ifndef DEBUG_FLAGS
#define DEBUG_FLAGS 0
#endif

#define DEBUG_FLAG_SCHEDULER   (1 << 0)
#define DEBUG_FLAG_TIMEREVENTS (1 << 1)

// Macros and functions for debugging
void printk(Char8 *message, ...) __attribute__((format(printf, 1, 2)));

#define DEBUG(flags, ...) if (flags & DEBUG_FLAGS) printk(__VA_ARGS__)

#define REPEAT_16(x) x x x x x x x x x x x x x x x x
#define PRINT_CONTEXT(c) printk("{\n" \
  "\tr0:  %lx\tr1:  %lx\tr2:  %lx\tr3:  %lx\n" \
  "\tr4:  %lx\tr5:  %lx\tr6:  %lx\tr7:  %lx\n" \
  "\tr8:  %lx\tr9:  %lx\tr10: %lx\tr11: %lx\n" \
  "\tr12: %lx\tsp:  %lx\tlr:  %lx\tpc:  %lx\n" \
  "\tCPSR: %lx\n" \
  "}\n", \
  (c).r0, (c).r1, (c).r2, (c).r3, (c).r4, (c).r5, (c).r6, (c).r7, \
  (c).r8, (c).r9, (c).r10, (c).r11, (c).r12, (c).sp, (c).lr, (c).pc, \
  (c).cpsr \
)

// Crash with a helpful message if Expr is false
#define ASSERT(Expr) \
  if (!(Expr)) { \
    printk("ASSERT: %s %s:%d\n", #Expr, __FILE__, __LINE__); \
    while (1); \
  }

// This is a goat-sacrifice to get compile-time assertions in C.
// If the predicate is false, it attempts to typedef an array with negative
// size.
// Some examples I've seen of this idea doubly invert the predicate with `!!`.
// I'm not sure why, so I haven't done that. If this stops working one day, you
// could try that.
#define COMPILE_ASSERT(predicate) typedef char foo[2*(predicate)-1];

#endif
