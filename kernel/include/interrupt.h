#ifndef __INTERRUPT_H__
#define __INTERRUPT_H__

/* Interface for registering interrupt handlers */

typedef void (*InterruptHandler)(Uintn irqNum);

/*
  Enable an IRQ line and register a handler for that interrupt
*/
StatusCode registerIrq(Uintn irqNum, InterruptHandler isr);

/* Begin platform-specific stuff. If we supported multiple platforms this stuff
   could be in a separate file and ifdef'd or something */

// These numbers can be found in the "* Integration" sections that the
// AM37x TRM has for each module
// (e.g. IRQ_TIMER1 is in section 16.2.3.3)
#define IRQ_TIMER1 37
#define IRQ_UART3  74
#define IRQ_UART   IRQ_UART3
#define NUM_IRQ_LINES 96

#endif
