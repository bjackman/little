#include <stdint.h>

// Base types and values for Little

#ifndef __BASE_H__
#define __BASE_H__

typedef uint_fast32_t Uintn;
#define UINTN_MAX UINT_FAST32_MAX
typedef int_fast32_t  Intn;
#define UINTN_MAX UINT_FAST32_MAX

typedef int8_t     Int8;
typedef int16_t    Int16;
typedef int32_t    Int32;
typedef int64_t    Int64;
 
typedef uint8_t    Uint8;
typedef uint16_t   Uint16;
typedef uint32_t   Uint32;

typedef char      Char8;

typedef Uintn      Boolean;
#define TRUE  1
#define FALSE 0

typedef uintptr_t  UintPtr;

#define SIZE_1K  0x0400
#define SIZE_4K  (SIZE_1K * 4)
#define SIZE_16K (SIZE_1K * 16)

#define SIZE_1M  (SIZE_1K * SIZE_1K)

#define ALIGNED_ON(val, alignment) ((((Uint32) (val)) & ((alignment) - 1)) == 0)

#endif