#ifndef __ALLOC_H__
#define __ALLOC_H__

#include <memoryType.h>

void *allocatePages(Uint32 size, MemoryType type);

void *kalloc(Uint32 size);
void kfree(void *allocation);

void kfree(void *buffer);

void initAllocator();

#endif
