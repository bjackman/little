#ifndef __COPYMEM_H__
#define __COPYMEM_H__

void *copyMem(void *destination, void *source, Uintn numBytes);

#endif
