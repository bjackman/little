#ifndef __FIFO_H__
#define __FIFO_H__

// TODO this thing could probably be faster if lastRead and
// lastWritten were pointers into buf rather than indices.
// Modular arithmetic would get awkward though.
typedef struct {
  Uintn lastRead;
  Uintn lastWritten;
  Char8 *buf;
} Fifo;

#define FIFO_INIT(f, buffer) do { \
  (f)->lastRead = 0;       \
  (f)->lastWritten = 0;    \
  (f)->buf = (buffer);     \
} while (0)                \

#define FIFO_IS_EMPTY(f) ((f)->lastRead == (f)->lastWritten)

// We never fill the fifo totally - we waste a character so we can tell between
// empty and full.
// The fifo is full when lastWritten = firstWritten - 1, modulo the fifo's size.
// This could be lastWritten == (lastRead - 1) % size but I think that only works when
// size divides UINTN_MAX
#define FIFO_IS_FULL(f, size) ((f)->lastWritten == ((f)->lastRead + (size) -1) % size)

#define FIFO_READ(f, size) ((f)->buf[(f)->lastRead = (f)->lastRead + 1 % size])

#define FIFO_WRITE(f, size, c) ((f)->buf[(f)->lastWritten = (f)->lastWritten + 1 % size] = (c))

#endif
