#ifndef __HASH_TABLE_H__
#define __HASH_TABLE_H__

#include <linkedList.h>

typedef struct _HashTableEntry {
  Uintn                              key;
  void                              *value;
  LIST_ENTRY(struct _HashTableEntry) link;
} HashTableEntry;

LIST_HEAD(BinHead, HashTableEntry);

/*
  Hmmm.. this structure looks a bit funny.
  The way we use it is to allocate something larger than sizeof HashTable.
  This allows us to have a variable number of bins.
*/
typedef struct {
  Uintn     numBins;
  BinHead   bins[0];
} HashTable;

#define EMPTY_HASH_TABLE(_numBins) { .numBins = _numBins, .bins = {} }
  
#define SIZE_OF_HASH_TABLE(numBins) \
  (sizeof(Uintn) + numBins * sizeof(BinHead)

void hashTableInit(HashTable *table, Uintn numBins);

void hashTableInsert(HashTable *table, HashTableEntry *entry);

void *hashTableGet(HashTable *table, Uintn requiredKey);

#endif
