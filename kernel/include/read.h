#ifndef __READ_H__
#define __READ_H__

/*
  Read a character from the file descriptor and put it in the return register
  of the current thread. If there is no character in the UART's input buffer,
  block the current thread and read the character at a later date.
*/
void readChar(Uintn fileDescriptor);

#endif
