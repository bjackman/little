/* Code to enable and disable interrupts at the CPU level */

/* Enable IRQs in the CPU */
void cpuEnableInterrupts();

/* Disable IRQs in the CPU */
void cpuEnableInterrupts();
