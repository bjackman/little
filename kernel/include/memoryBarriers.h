/* Functions to trigger memory barriers 
   http://www.rdrop.com/users/paulmck/scalability/paper/whymb.2010.07.23a.pdf */

/* Wait for all memory accesses to complete */
void dataSyncBarrier();
