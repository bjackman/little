#ifndef __SCHEDULER_H__
#define __SCHEDULER_H__

#include <context.h>
#include <errors.h>

/*
  Add a user thread with threadMain as its starting point.
  threadMain will be called with the threads ID and entryArg as its arguments.
*/
StatusCode addUserThread(void (*threadMain)(), void *entryArg);

/*
  Notify the scheduler that the currently executing thread has finished
*/
StatusCode exitCurrentThread();

void schedule(Uintn msSinceLastSchedule);

/*
  Get the context for thread 0.
  This is to be used at boot.
*/
ArmContext *getInitContext();

/*
  Notfy the scheduler that the current thread has caused an error and that
  instead of restoring it, we should call a signal handler with the provided
  argument.
*/
void deferSignalHandlerCall(SignalHandler handler, Uint32 argument);

/*
  Cause the current thread to block for waitTimeMs seconds
*/
StatusCode waitCurrentThread(Uint64 waitTimeMs);

/*
  Remove the current thread from the ready queue indefinitely.
  Return an opaque identifier for the removed thread, that can be passed to
  unblockThread.
*/
void *blockCurrentThread();

void unblockThread(void *thread);

/*
  Initialise scheduler state
*/
void schedulerInit();

#endif
