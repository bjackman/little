#ifndef __BOOT_H__
#define __BOOT_H__

#include <pageTable.h>

// Definitions needed for the boot process

void usrEntry();

void reset();

void goToUsr() __attribute__((noreturn));

void setTranslationTableBase(MasterTranslationTableEntry *masterTable);

// This symbol is defined by the linker - only its address should be accessed
// (i.e. you should only see &exceptionVectors, with the ampersand).
extern int exceptionVectors;

void *setVectorBaseAddress(void *vectors);

void enableMmu();

void cpuEnableInterrupts();

// This symbol is defined by the linker - only its address should be accessed
// (i.e. you should only see &gUsrAreaStart, with the ampersand).
extern int gUsrAreaStart;

#endif
