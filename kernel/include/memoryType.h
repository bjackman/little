#ifndef __MEMORY_TYPE_H__
#define __MEMORY_TYPE_H__

typedef enum {
  userData,
  kernelData,
  maxMemoryType
} MemoryType;

#endif
