#ifndef __MEMORY_MAP_H__
#define __MEMORY_MAP_H__

// Memory map - this is for the ARM Versatile Express Cortex-A9

#define CS7              0x10000000

#define UART0            (CS7 + 0x9000)

#endif