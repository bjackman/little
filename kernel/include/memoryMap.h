#ifndef __MEMORY_MAP_H__
#define __MEMORY_MAP_H__

// Memory map for BeagleBoard xM

#define UART3_BASE          0x49020000

// Can be programmed to access flash or SRAM
#define AM37X_GPMC_BASE     0x00000000
#define AM37X_GPMC_SIZE     (AM37X_PERIPH_BASE - 1)
// All the peripherals are conveniently next to each other
#define AM37X_PERIPH_BASE   0x40000000
#define AM37X_PERIPH_SIZE   (AM37X_DRAM_BASE - AM37X_PERIPH_BASE - 1)
// Next comes RAM
#define AM37X_DRAM_BASE     0x80000000
#define AM37X_DRAM_SIZE     0x20000000
// ... and after that, nothing.

#define UART3_BASE          0x49020000
// Beagle xM System Reference Manual section 4.18 says the connector is wired up
// to UART3.
#define UART_BASE  UART3_BASE

#define GPTIMER1_BASE       0x48318000

#define INTC_BASE           0x48200000

#define GPTIMER1_BASE       0x48318000

/*
  Values defined by the linker.
  Only ever reference these using the address operator (&).
*/

extern int gAllocatableMemoryStart;

#define AM37X_DRAM_TOP (AM37X_DRAM_BASE + AM37X_DRAM_SIZE)
#define ALLOCATABLE_MEMORY_SIZE (AM37X_DRAM_SIZE - &gAllocatableMemoryStart)

#endif
