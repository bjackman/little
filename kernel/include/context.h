#ifndef __CONTEXT_H__
#define __CONTEXT_H__

#include <debug.h>

// Structure to represent a user context.
// Note that assembly code vectors.s and context.s is heavily dependent on this
// structure's layout.
#pragma pack(1)
typedef struct {
  Uint32 cpsr;
  Uint32 r0;
  Uint32 r1;
  Uint32 r2;
  Uint32 r3;
  Uint32 r4;
  Uint32 r5;
  Uint32 r6;
  Uint32 r7;
  Uint32 r8;
  Uint32 r9;
  Uint32 r10;
  Uint32 r11;
  Uint32 r12;
  Uint32 sp;
  Uint32 lr;
  Uint32 pc;
} ArmContext;
#pragma pack()

/*
  The "current context buffer" refers to the ArmContext buffer of the task
  currently being executed.
  When the context needs to be saved, that is the buffer it should be saved to.
  When we go back to user mode, that buffer contains the context to restore.
*/
void setCurrentContextBuffer(ArmContext *context);

ArmContext *gCurrentContextBuffer;

#define CPSR_MODE_USER 0x10

// A "default" CPSR for user mode. Everything is zero except the mode bits
#define DEFAULT_USER_CPSR (0x0 | CPSR_MODE_USER)

#endif
