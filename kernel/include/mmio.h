#ifndef __MMIO_H__
#define __MMIO_H__

void mmioWrite8(Uintn addr, Uint8 val);

Uint8 mmioRead8(Uintn addr);

void mmioWrite32(Uintn addr, Uint32 val);

Uint32 mmioRead32(Uintn addr);

#endif
