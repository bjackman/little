#ifndef __INTERRUPT_CONTROLLER_H__
#define __INTERRUPT_CONTROLLER_H__

/* Interface for an interrupt controller driver */

/*
  Unmask an interrupt in the interrupt controller
*/
void enableInterrupt(Uintn interruptNumber);

/*
  When an interrupt has been received, get which IRQ line is active
*/
Uintn getActiveIrq();

/*
  Signal to the hardware that the current interrupt has been handled and the
  next one can be signalled, if any
*/
void signalEndOfInterrupt();

#endif
