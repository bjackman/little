#include <scheduler.h>
#include <read.h>
#include <fifo.h>
#include <uart.h>
#include <context.h>

static Intn *fReadCharDestination;

static void unblockOnCharReady(void *thread) {
  ASSERT(!FIFO_IS_EMPTY(&gUartReadFifo));
  ASSERT(fReadCharDestination != NULL);

  *fReadCharDestination = (Intn) FIFO_READ(&gUartReadFifo,
                                               UART_READ_BUFFER_SIZE);
  unblockThread(thread);
}

void readChar(Uintn fd) {
  // Don't see why this would be called when no thread is running..
  ASSERT(gCurrentContextBuffer != NULL);

  // The only valid file descriptors are the standard streams.
  // All of them are the UART.
  if (fd < 3) {
    if (FIFO_IS_EMPTY(&gUartReadFifo)) {
      // No character pending, block the thread until one is received
      fReadCharDestination = (Intn *) &gCurrentContextBuffer->r0;
      void *thread = blockCurrentThread();
      setUartReceiveEvent(unblockOnCharReady, thread);
    } else {
      // Return the character to the current thread
      gCurrentContextBuffer->r0 = (Intn) FIFO_READ(&gUartReadFifo,
                                                   UART_READ_BUFFER_SIZE);
    }
  } else {
    // Return an error to the current thread.
    gCurrentContextBuffer->r0 = -1;
  }
}
