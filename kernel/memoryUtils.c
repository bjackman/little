// Utility functions for manipulating memory

// Zero a 32-bit aligned region of memory
void zeroMem32(void *start, Uint32 numBytes) {
  Uint32 *mem = (Uint32 *) start;
  Uint32 i;
  for (i = 0; i < (numBytes /4); i++) {
    mem[i] = 0;
  }
}