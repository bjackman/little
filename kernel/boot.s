/* Functions to be used during initialisation */

#include <armCpuModes.h>

/* Code to run first, branched to on reset.
   Initialise stacks for each mode then branch to C.
   ARM CPSR mode bits:
     0000 User
     0001 FIQ
     0010 IRQ
     0011 Supervisor
     0111 Abort
     1011 Undefined
     1111 System
*/
.global reset
reset:
  @ Set up stacks. We currently only have a single stack for all privileged
  @ modes. When changing modes, this kernel stack should be empty.
  ldr sp, =gKernelStackBase
  cps #MODE_ABORT
  ldr sp, =gKernelStackBase
  cps #MODE_IRQ
  ldr sp, =gKernelStackBase
  cps #MODE_UNDEFINED
  ldr sp, =gKernelStackBase
  cps #MODE_SVC

  ldr lr, =restoreUserContext
  b   kernelCInit

/* Enable the MMU */
.global enableMmu
enableMmu:
  mcr p15, 0, r1, c8, c7, 0    @ Invalidate TLB
  ldr r0, =0x55555555
  mcr p15, 0, r0, c3, c0, 0    @ Set DACR to all "manager" - no permissions checking
  mrc p15, 0, r0, c1, c0, 0
  orr r0, r0, #1
  mcr p15, 0, r0, c1, c0, 0
  mov pc, lr

/* Set the Translation Table Base Register (TTBR).
   Function signature:
    void setTranslationTableBase (MasterTranslationTableEntry *masterTable)
*/
.global setTranslationTableBase
setTranslationTableBase:
  mcr p15, 0, r0, c2, c0, 0
  mov pc, lr

/* Set the Vector Base Address Register (VBAR) to the value in r0
   Return the old VBAR in r0 */
.global setVectorBaseAddress
setVectorBaseAddress:
  mrc p15, 0, r1, c12, c0, 0
  mcr p15, 0, r0, c12, c0, 0
  mov r0, r1
  mov pc, lr
