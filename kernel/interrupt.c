#include <interrupt.h>
#include <interruptController.h>
#include <context.h>

/* Core interrupt handling logic */

// Map of interrupt numbers to interrupt handlers
static InterruptHandler fInterruptHandlers[NUM_IRQ_LINES] = { NULL };

/*
  Enable an IRQ line and register a handler for that interrupt
*/
StatusCode registerIrq(Uintn irqNum, InterruptHandler isr) {
  fInterruptHandlers[irqNum] = isr;
  enableInterrupt(irqNum);
  return SUCCESS;
}

/*
  Function called when an interrupt is taken. Finds and calls the service
  routine for the active IRQ.
  Returns a pointer to the Context structure that should subsequently be
  restored.
*/
void onInterrupt(ArmContext *context) {
  Uintn activeIrq = getActiveIrq();
  ASSERT(fInterruptHandlers[activeIrq] != NULL);
  fInterruptHandlers[activeIrq](activeIrq);
  signalEndOfInterrupt();
}
