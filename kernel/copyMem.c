#include <copyMem.h>

// Naive implementation. Unless the compiler is quite clever we can probably
// make this much faster in assembly.
// Destination and source buffers shouldn't overlap.
void *copyMem(void *destination, void *source, Uintn numBytes) {
  // Check for overlap (unsigned overflow will solve case where dest < src)
  ASSERT((UintPtr) destination - (UintPtr) source > numBytes);

  // Use Uint8 pointers because you aren't supposed to do arithmetic on void
  // pointers in C.
  Uint8 *dest = destination;
  Uint8 *src = source;

  for (Uintn i = 0; i < numBytes; i++) {
    *(dest++) = *(src)++;
  }

  // Really we just do this because memcpy does.
  return destination;
}
