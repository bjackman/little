.global getDataFaultAddress
getDataFaultAddress:
  mrc p15, 0, r0, c6, c0, 0
  mov pc, lr

.global getDataFaultStatus
getDataFaultStatus:
  mrc p15, 0, r0, c5, c0, 0
  mov pc, lr


.global getInstructionFaultStatus
getInstructionFaultStatus:
  mrc p15, 0, r0, c5, c0, 1
  mov pc, lr
