/* Exception vectors */

#include <armCpuModes.h>
#define SIZE_OF_ARM_CONTEXT (17 * 4)

/*
  Save the context we came from into the current context buffer.
  See context.h for what that means.
*/
.macro SAVE_CONTEXT
  push  {r2-r3}
  ldr   r2, =gCurrentContextBuffer
  ldr   r2, [r2]
  cmp   r2, #0          @ check if gCurrentContextBuffer is NULL (which means we were idling)
  popeq {r2-r3}
  beq   0f
  mrs   r3, SPSR
  str   r3, [r2], #4    @ save CPSR
  str   lr, [r2, #(15 * 4)] @ save PC
  str   r0, [r2], #4    @ save R0
  mov   r0, r2
  pop   {r2-r3}
  stm   r0, {r1-r14}^
0:
.endm

/* A macro for attempting to automatically detect stack leaks. */
.macro CHECK_STACK_LEAK
#ifdef DEBUG_BUILD
  push  {r0-r3}
  add   r2, sp, #(4 * 4) @ Adjust for the push we just did
  ldr   r1, =gKernelStackBase
  cmp   r2, r1           @ Check if the SP has changed
  beq   0f
  ldr   r0, =stackLeakDebugFormat
  mrs   r3, CPSR
  bl    printk           @ The SP has changed. Print a message.
                         @ (r1 and r2 are old and new SP respectively)
0:
  pop   {r0-r3}
#endif
.endm

/* Functions called here are defined in errors.c */
.global exceptionVectors
exceptionVectors:
  b   reset
  b   _undefinedInstruction
  b   _supervisorCall
  b   _prefetchAbort
  b   _dataAbort
  b   .
  b   _irq
  b   fastInterrupt

_undefinedInstruction:
  CHECK_STACK_LEAK
  SAVE_CONTEXT
  bl undefinedInstruction
  b  restoreUserContext

_prefetchAbort:
  CHECK_STACK_LEAK
  sub   lr, lr, #4                      @ Correct return address
  SAVE_CONTEXT
  bl prefetchAbort
  b     restoreUserContext

_dataAbort:
  CHECK_STACK_LEAK
  sub   lr, lr, #8                      @ Correct return address
  SAVE_CONTEXT
  bl dataAbort
  b     restoreUserContext              @ Restore the context returned by onInterrupt

_supervisorCall:
  CHECK_STACK_LEAK
  push  {r2-r3}                      @ Push r2 and r2 as they are syscall args
  ldr   r2, =gCurrentContextBuffer
  ldr   r2, [r2]
  mrs   r3, SPSR
  str   r3, [r2], #(5 * 4)           @ save CPSR, skip clobber registers
  str   lr, [r2, #(11 * 4)]          @ save PC (return address)
  stm   r2, {r4-r14}^                @ save other non-clobber registers
  pop   {r2-r3}                      @ pop syscall args

  ldr   r5, =(gMaxSyscallNumber)     @ Validate syscall number
  ldr   r5, [r5]
  cmp   r0, r5
  bhi   0f
  ldr   r4, =syscallHandlers         @ We'll now use the syscallHandlers table as a jump table.
  mov   lr, pc
  ldr   pc, [r4, r0, lsl #2]
  b     restoreUserContext
0:                                   @ System call was invalid!
  bl    invalidSyscall               @ Send signal
  b     restoreUserContext

_irq:
  CHECK_STACK_LEAK
  sub   lr, lr, #4                      @ Correct return address
  SAVE_CONTEXT
  bl    onInterrupt                     @ Handle interrupt
  b     restoreUserContext              @ Restore the context returned by onInterrupt


#ifdef DEBUG_BUILD
stackLeakDebugFormat:
  .string "STACK LEAK? %x -> %x (CPSR = %x)\n"
#endif
