#include <hashTable.h>
#include <linkedList.h>

static Uintn hash(Uintn key) {
  // TODO LOL
  return 0;
}

void hashTableInit(HashTable *table, Uintn numBins) {
  for (Uintn i = 0; i < numBins; i++) {
    LIST_INIT(table->bins + i);
  }
}

void hashTableInsert(HashTable *table, HashTableEntry *entry) {
  Uintn index = hash(entry->key);

  BinHead *binHead = &table->bins[index];

  LIST_INSERT_HEAD(binHead, entry, link);
}

void *hashTableGet(HashTable *table, Uintn requiredKey) {
  Uintn index = hash(requiredKey);

  LIST_FOREACH_UNSAFE(table->bins + index, HashTableEntry, item, link) {
    if (item->key == requiredKey) {
      return item->value;
    }
  }

  return NULL;
}
