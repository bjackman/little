#include <memoryMap.h>
#include <mmio.h>
#include <interrupt.h>
#include <timer.h>

/*
  Driver for the AM37x General Purpose Timers.   See section 16 of the AM73x
  Technical Reference Manual for a description, particularly section 16.2.4.
*/

#define GPT1_READ(offset)       mmioRead32(GPTIMER1_BASE + offset)
#define GPT1_WRITE(offset, val) mmioWrite32(GPTIMER1_BASE + offset, val)

#define TIOCP_CFG 0x10
#define TIOCP_SOFTRESET (1 << 1)

#define TISTAT 0x14
#define TISTAT_RESETDONE 1

// Timer Interrupt Status Register
#define TISR 0x18
// - Match interrrupt status
#define TISR_MAT_IT_FLAG  1
#define TISR_OVF_IT_FLAG (1 << 1)

// Timer Interrupt Enable Register
#define TIER 0x1C
// - Match Interrupt Enable
#define TIER_MAT_IT_ENA  1
// - Overflow Interrupt Enable
#define TIER_OVF_IT_ENA (1 << 1)

// Timer Control Regiseter
#define TCLR 0x24
// Trigger Output Mode
#define TCLR_TRG_OVERFLOW           (0x1 << 10)
#define TCLR_TRG_OVERFLOW_AND_MATCH (0x2 << 10)
// Compare Enable
#define TCLR_CE                     (1 << 6)
// Autoreload enable (causes the timer to reload from the TLDR on overflow)
#define TCLR_AR                     (1 << 1)
// Timer Start bit
#define TCLR_ST                      1

// Timer Counter Register
#define TCRR 0x28

// Timer Load Register - the value reloaded into the TCRR after overflow
#define TLDR 0x2C

// Timer Trigger Register
#define TTGR 0x30

// Timer Match Register
#define TMAR 0x38

/* The following registers aren't supported by QEMU */

// Timer negative- and positive-increment registers
#define TPIR 0x48
#define TNIR 0x4C

// Timer Overflow Wrapping registers - the number of overflows required to
// generate an interrupt.
#define TOWR 0x58


#define TIMER_INPUT_HZ   0x8000
#define TIMER_INPUT_KHZ  (TIMER_INPUT_HZ / 1000)

// The desired frequency of interrupts
#define SYSTEM_TICK_MS   100

static TimerCallback fTimerCallback = NULL;

/*
  Interrupt service routine for a timer interrupt.
  Calls the callback passed to timerInit, passing the number of milliseconds
    since the last tick.
*/
static void timerInterrupt(Uintn irqNum) {
  ASSERT(irqNum == IRQ_TIMER1);
  ASSERT(fTimerCallback != NULL);

  // Assert that we got a overflow interrupt as that is the only type we enabled
  Uint32 interrupts = GPT1_READ(TISR);
  ASSERT(interrupts == TISR_OVF_IT_FLAG);

  // Acknowledge the interrupt (so it goes away)
  GPT1_WRITE(TISR, TISR_OVF_IT_FLAG);

  fTimerCallback(SYSTEM_TICK_MS);
}

/*
  Initialise the timer hardware and register an interrupt handler
*/
void timerInit(void (*callback)()) {
  fTimerCallback = callback;

  // Initiate a soft reset
  GPT1_WRITE(TIOCP_CFG, TIOCP_SOFTRESET);
  // Wait until the reset is done
  while (!(GPT1_READ(TISTAT) & TISTAT_RESETDONE)) {
    ;
  }

  // Set the timer load value so that the timer overflows every SYSTEM_TICK_MS
  // milliseconds
  // MEGA FUCKING TODO
  GPT1_WRITE(TLDR, UINT32_MAX - (TIMER_INPUT_KHZ * SYSTEM_TICK_MS));

  // Reset the timer counter to the value in the timer load register (TLDR)
  // (It doesn't matter what value we write)
  GPT1_WRITE(TTGR, 0);

  // Claim the IRQ and register a handler
  registerIrq(IRQ_TIMER1, timerInterrupt);

  // Clear any "leftover" interrupts
  GPT1_WRITE(TISR, 0x7);

  GPT1_WRITE(TCLR,
    TCLR_TRG_OVERFLOW | // Trigger an interrupt when the timer overflows
    TCLR_AR |           // Load the timer count from TLDR on overflow
    TCLR_ST             // Start the timer
  );

  // Enable the timer to produce interrupts
  GPT1_WRITE(TIER, TIER_OVF_IT_ENA);
}
