#include <mmio.h>
#include <interruptController.h>
#include <interrupt.h>
#include <memoryMap.h>
#include <memoryBarriers.h>

// Driver for the interrupt controller in the TI AM37x
// Probably compatible with OMAP and other Texas Instruments stuff too.

#define INTC_WRITE(offset, val) mmioWrite32(INTC_BASE + offset, val)
#define INTC_READ(offset)       mmioRead32(INTC_BASE + offset)

// Registers masked in the MIR are disabled
#define INTCPS_MIR(n) (0x84 + 0x20 * (n))
// Address to write to clear the mask on interrupt n
#define INTCPS_MIR_CLEAR(n) (0x88 + 0x20 * (n))

// Holds the raw interrupt state before marking
#define INTCPS_ITR(n) (0x80 + 0x20 * (n))

#define INTCPS_CONTROL      0x48
// Bit to write to signal that the current interrupt is finished
#define INTCPS_CONTROL_NEWIRQAGR 1

// The SIR holds the spurious interrupt flag and the active IRQ number
#define INTCPS_SIR_IRQ 0x40
#define INTCPS_SIR_IRQ_ACTIVEIRQ_MASK 0x7F

/*
  Unmask an interrupt
*/
void enableInterrupt(Uintn interruptNumber) {
  ASSERT (interruptNumber < NUM_IRQ_LINES);

  // The interrupt mask is spread across 3 MIR registers - work out which one
  // we need, and which bit of that register we need.
  Uintn mirIndex = interruptNumber / 32;
  Uintn bitIndex = interruptNumber % 32;
  INTC_WRITE(INTCPS_MIR_CLEAR(mirIndex), (1 << bitIndex));
}

/*
  When an interrupt has been received, get which IRQ line is active
*/
Uintn getActiveIrq() {
  Uintn irqNum = INTC_READ(INTCPS_SIR_IRQ) & INTCPS_SIR_IRQ_ACTIVEIRQ_MASK;
  return irqNum;
}

/*
  Signal to the hardware that the current interrupt handler is finished
*/
void signalEndOfInterrupt() {
  INTC_WRITE(INTCPS_CONTROL, INTCPS_CONTROL_NEWIRQAGR);

  // See AM37x Technical Reference Manual section 12.5.2
  // The interrupt service routine will probably have done something to end
  // the interrupt condition on the device side (e.g. maybe the device has an
  // "interrupt acknowledge" register). If we wrote the NEWIRQAGR bit before
  // the device deasserts its interrupt line, we would have the same interrupt
  // handled twice. We therefore do a Data Synchonisation Barrier to make sure
  // all memory access performed by the ISR is finished.
  // ... Actually, the example in the TRM does its barrier *after* writing the
  // NEWIRQAGR bit. I guess that's OK, since we're going to have interrupts
  // disabled at this point anyway.
  dataSyncBarrier();
}
