#include <uart.h>
#include <mmio.h>
#include <memoryMap.h>
#include <interrupt.h>
#include <fifo.h>

// Driver for UARTs in OMAP systems

/*
  Some notes about this UART system:

  It has 3 disjoint "register access modes", in which different sets
  of registers are available. See AM37x TRM section 19.4.3.2.1 for
  which registers are accessible in which modes. Use LCR_REG to select modes.
*/

#define UART_WRITE(offset, val)  mmioWrite8(UART_BASE + offset, val)
#define UART_READ(offset)        mmioRead8 (UART_BASE + offset)

// Transmit Holding Register
#define UART_THR_REG            0x0

// Receive Holding Register (same address as THR)
#define UART_RHR_REG            0x0

// Interupt Enable Register
#define UART_IER_REG            0x4
#define UART_IER_RHR_IT         (1 << 0)
#define UART_IER_THR_IT         (1 << 1)

// Line Control Register: used to set the register access mode
#define UART_LCR_REG            0xC
#define UART_LCR_DIV_EN         (1 << 7)

// Line Status Register
#define UART_LSR_REG            0x14
// Says if there's data in the RHR
#define UART_LSR_RX_FIFO_E       (1 << 0)

// Supplementary Status Register
#define UART_SSR_REG            0x44
#define UART_SSR_TX_FIFO_FULL   1

// Line Control Register: used to set the register access mode
#define UART_LCR_REG            0xC
#define UART_LCR_DIV_EN     (1 << 7)

// Interupt Enable Register
#define UART_IER_REG            0x4
#define UART_IER_RHR_IT         (1 << 0)
#define UART_IER_THR_IT         (1 << 1)

static struct {
  void *callback;
  void *context;
} fReceiveEvent;

static void writeChar(const Char8 c) {
  // Wait until the Tx fifo has space
  // (TODO obviously this is like super slow)
  while (UART_READ(UART_SSR_REG) & UART_SSR_TX_FIFO_FULL) {
    ;
  }
  UART_WRITE(UART_THR_REG, c);
}

void uartPrint(const Char8 *str) {
  while(*str) {
    writeChar(*(str++));
  }
}

void uartWrite(const Char8 *str, Uintn length) {
  for (Uintn i = 0; i < length; i++) {
    writeChar(str[i]);
  }
}

Fifo  gUartReadFifo;
Char8 uartReadFifoBuffer[UART_READ_BUFFER_SIZE];

#define UART_FIFO_READ(f) FIFO_READ(f, UART_READ_FIFO_SIZE)
#define UART_FIFO_WRITE(f, c) FIFO_WRITE(f, UART_READ_BUFFER_SIZE, c)

static void interruptHandler() {
  while (UART_READ(UART_LSR_REG) & UART_LSR_RX_FIFO_E) {
    // Get the character
    Char8 c = (Char8) UART_READ(UART_RHR_REG);
    if (!FIFO_IS_FULL(&gUartReadFifo, UART_READ_BUFFER_SIZE)) {
      UART_FIFO_WRITE(&gUartReadFifo, c);
    } else {
      printk("WARNING: UART FIFO full, dropping chars\n");
    }

    // Trigger the receive event if there is one
    if (fReceiveEvent.callback != NULL) {
      // Save and delete the callback - it only gets triggered once.
      // (We need to delete it before calling it, in case the callback calls
      //  setUartReceiveEvent)
      UartCallback callback = fReceiveEvent.callback;
      fReceiveEvent.callback = NULL;

      callback(fReceiveEvent.context);
    }
  }
}


void setUartReceiveEvent(UartCallback callback, void *context) {
  // There is only one receive event. If it gets set twice, something is probably wrong.
  ASSERT(fReceiveEvent.callback == NULL);

  fReceiveEvent.callback = callback;
  fReceiveEvent.context = context;
}

void uartInit() {
  // We assume the bootloader has already set up the UART for us.
  // If it hasn't, check out the AM37x TRM secion 19.5.1.1

  fReceiveEvent.callback = NULL;

  FIFO_INIT(&gUartReadFifo, uartReadFifoBuffer);

  registerIrq(IRQ_UART, interruptHandler);

  // Go into Operational Mode so we can access IER_REG
  Uint32 SavedLcr = UART_READ(UART_LCR_REG);
  UART_WRITE(UART_LCR_REG, SavedLcr & ~UART_LCR_DIV_EN);

  // Enable the RHR interrupt (which says there's data received)
  UART_WRITE(UART_IER_REG, UART_IER_RHR_IT);
}
