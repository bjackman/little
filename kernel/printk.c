#include <stdarg.h>
#include <uart.h>

#define MAX_OUTPUT_STRING_LENGTH 1024

// Convert a number (less then 0x10) to its ASCII representation
#define NUMBER_TO_CHAR(num) ((num) <= 9 ? '0' + (num) : 'a' - 10 + (num))

/*
  Helper function to compute the number of decimal digits required to display a
  number
*/
static Uintn computeDecimalWidth(Uintn num) {
  Uintn width = 0;
  do {
    width++;
    num /= 10;
  } while (num);
  return width;
}

/*
  Implementation of the actual printf logic.
  We can't really allocate heap memory in this function, as we might be
  using it to debug the memory allocator. So we just have a fixed-size buffer
  on the stack.

  Print the output string into `output` buffer. Refuse to write more than
  `length` bytes.

  Right now instead of truncating the output we just error out.
  TODO: Truncate the output properly and behave like POSIX.
*/
static void _snPrintf(Char8 *output, Uintn length,
                      Char8 *format, va_list vaArgs) {
  Uintn formatCounter = 0, outputCounter = 0;

  // Loop over the format string, building the output string as we go.
  // Break if the output string gets too big for the buffer.
  while (outputCounter < length - 1 && format[formatCounter]) {
    if (format[formatCounter] != '%') {
      // Print a normal characeter
      output[outputCounter++] = format[formatCounter++];
    } else {
      // We found a format specifier
      formatCounter++;

      if (format[formatCounter] == 'l') {
        // 'l' is a modifier that, as far as I can tell, makes no difference
        // on 32-bit ARM (i.e. I believe long unsigned int == unsigned int),
        // but that GCC's -Wformat requires for Uint32s.
        // Allow, but ignore it.
        formatCounter++;
      }

      switch (format[formatCounter++]) {
        Uintn   num;
        Uintn   shift;
        UintPtr ptr;
        Uintn   width;
        Uintn   i;
        Char8   *str;
        Char8   chr;
        case 'd':
          //
          // Display an unsigned decimal integer
          //
          num = va_arg(vaArgs, Uintn);

          // Work out the width of the number
          width = computeDecimalWidth(num);
          outputCounter += width;
          if (outputCounter >= length) {
            goto error;
          }

          // Display each character, right-to-left
          i = 1;
          do {
            output[outputCounter - (i++)] = NUMBER_TO_CHAR(num % 10);
            num /= 10;
          } while (num > 0 && outputCounter < length);
          break;

        case 'p':
          //
          // Display an unsigned hexadecimal integer
          //
          ptr = va_arg(vaArgs, UintPtr);

          // The maximum number of characters required to print a Uintn in
          // hexadecimal: two characters per byte.
          // TODO: we're just going to print the full width.
          width = (sizeof (Uintn) * 2);
          if (outputCounter + width >= length) {
            goto error;
          }

          shift = width * 4;
          while (shift) {
            shift -= 4;
            output[outputCounter++] = NUMBER_TO_CHAR((ptr >> shift) & 0xF);
          }
          break;

        case 'x':
          //
          // Display an unsigned hexadecimal integer
          //
          num = va_arg(vaArgs, Uintn);

          // The maximum number of characters required to print a Uintn in
          // hexadecimal: two characters per byte.
          // TODO: we're just going to print the full width.
          width = (sizeof (Uintn) * 2);
          if (outputCounter + width >= length) {
            goto error;
          }

          shift = width * 4;
          while (shift) {
            shift -= 4;
            output[outputCounter++] = NUMBER_TO_CHAR((num >> shift) & 0xF);
          }
          break;

        case 's':
          //
          // Display a string
          //
          str = va_arg(vaArgs, Char8 *);

          while (*str) {
            if (outputCounter >= length) {
              goto error;
            }
            output[outputCounter++] = *(str++);
          }
          break;
        case 'c':
          //
          // Display a Char8
          //
          chr = va_arg(vaArgs, int);

          output[outputCounter++] = chr;
          break;
        default:
          // Unrecognised format specifier
          uartPrint("BAD PRINTK FORMAT\n");
          goto error;
      }
    }
  }

  output[outputCounter] = '\0';
  return;

error:
  uartPrint("PRINTK ERROR\n");
  va_end(vaArgs);
}

/*
  The externally visible version of printk just calls va_start, then calls the
  internal _printk function. This allows us to call _printk from another
  variadic function.
*/
void printk(Char8 *format, ...) {
  Char8 output[MAX_OUTPUT_STRING_LENGTH];
  va_list vaArgs;

  va_start(vaArgs, format);
  _snPrintf(output, MAX_OUTPUT_STRING_LENGTH, format, vaArgs);
  va_end(vaArgs);

  uartPrint(output);
}

/*
  Externally visible snprintf, similarly just calls the internal one.
*/
void snPrintk(Char8 *output, Uintn length, Char8 *format, ...) {
  va_list vaArgs;

  va_start(vaArgs, format);
  _snPrintf(output, MAX_OUTPUT_STRING_LENGTH, format, vaArgs);
  va_end(vaArgs);
}
