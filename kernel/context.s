/*
  Restore a user context pointed to by gCurrentContextBuffer.
  It should conform to the ArmContext structure in context.h.

  Note that this *only* works for user contexts as it explcitly restores to the
  *user mode* SP and LR.

  All these special LDM instructions (with ^s) are a bit hairy.
  - With PC in reglist, it's a return-from-exception
  - Without PC in the reglist, it loads to user-mode registers. No writeback
    allowed.
  See the ARM A-R-M for more info.

  Also note the use of the link register for calculations. We cannot restore all
  registers in a single LDM instruction. If we LDM *all* registers, it will be
  a return-from-exception LDM. This loads all registers *then* changes mode, so
  the wrong mode's SP and LR get restored to. So we have to load general-purpose
  registers first, then the LR and SP, then do return from the exception.
  Loading the general purpose registers clobbers our pointer, so we back it up
  in the LR.
*/
.global restoreUserContext
restoreUserContext:
  ldr   lr, =gCurrentContextBuffer
  ldr   lr, [lr]
  cmp   lr, #0               @ If gCurrentContextBuffer is NULL, idle.
  bne   0f
  bl    cpuEnableInterrupts
  wfi
0:
  ldr   r1, [lr], #4         @ Load the CPSR
  msr   SPSR, r1
  ldmfd lr, {r0-r14}^        @ Restore user registers
  add   lr, lr, #(15 * 4)    @ Point LR to saved PC
  ldm   lr, {pc}^            @ Branch and restore CPSR
