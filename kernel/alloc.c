#include <pageTable.h>
#include <linkedList.h>
#include <memoryType.h>
#include <memoryMap.h>
#include <hashTable.h>

/*
  These structures hold info about allocations.
  The address of the allocated memory is the in addressIndexEntry.key.
  They are specially allocated in allocateAllocationStruct.
*/
typedef struct _Allocation {
  HashTableEntry          addressIndexEntry;
  Uint32                  size;
  LIST_ENTRY(struct _Allocation) link;
} Allocation;

LIST_HEAD(AllocationHead, Allocation);
static AllocationHead allocations;

/*
  This hash table allows us to look up allocation info by the address of the
  allocated memory.
*/
#define ALLOC_ADDRESS_INDEX_NUM_BINS 32
struct {
  HashTable table;
  BinHead   tableBins[ALLOC_ADDRESS_INDEX_NUM_BINS];
} addressIndex;

// The address of the next page to allocate
static UintPtr fNextAllocPage;

/*
  Allocate some pages of memory. The size provided must be a multple of the page
  size.
*/
void *allocatePages(Uint32 size, MemoryType type) {
  ASSERT(ALIGNED_ON(size, PAGE_SIZE));
  ASSERT(type < maxMemoryType);

  // We need to check that there is enough memory left. That means adding
  // size to fNextAllocPage and checking if it's gone past the end of RAM. That seems
  // pretty likely to overflow. Unsigned overflow is defined, so we'll just
  // check for that case.
  if (fNextAllocPage + size > AM37X_DRAM_TOP || fNextAllocPage + size < fNextAllocPage) {
    return NULL;
  }

  void *ret = (void *) fNextAllocPage;
  fNextAllocPage += size;

  setMemoryType(ret, size, type);

  return (void *) ret;
}

/*
  We store allocation metadata in Allocation structs. These are store in a
  list, and manually allocated
*/
static void *allocateAllocationStruct() {
  // Get the allocation struct we're going to return
  Allocation *ret = LIST_FIRST(&allocations);

  // Work out which one we'll choose next time
  // (Either immediately following `ret`, or if that would cross a page
  //  boundary, in a totally new page)

  Allocation *nextAllocStruct = (Allocation *) ((Uint8 *)ret) + sizeof(Allocation);
  // Check if we've gone past a page bondary
  if ((UintPtr) nextAllocStruct > NEXT_PAGE_BOUNDARY(ret) ||
      (UintPtr) nextAllocStruct < (UintPtr) ret /* Check for overlow */) {
    // Allocate another page for allocation structs
    nextAllocStruct = allocatePages(PAGE_SIZE, kernelData);
    if (nextAllocStruct == NULL) {
      return NULL;
    }
  }

  LIST_INSERT_HEAD(&allocations, nextAllocStruct, link);
  return ret;
}

void *kalloc(Uint32 size) {
  ASSERT(size > 0);

  Allocation *allocation = allocateAllocationStruct();
  if (allocation == NULL) {
    printk("Failed to allocate allocation structure\n");
    return NULL;
  }

  // TODO index allocations by size so we can re-use pages

  void *memory = allocatePages(ROUND_UP(size, PAGE_SIZE), kernelData);
  if (memory == NULL) {
    printk("Kernel failed to allocate %d bytes\n", (Uintn) size);
    return NULL;
  }

  allocation->addressIndexEntry.key   = (UintPtr) memory;
  allocation->addressIndexEntry.value = allocation;


  hashTableInsert(&addressIndex.table, &allocation->addressIndexEntry);

  return memory;
}

void kfree(void *buffer) {
  // todo lol
}

void initAllocator() {
  fNextAllocPage = (UintPtr) &gAllocatableMemoryStart;
  ASSERT(ALIGNED_ON(fNextAllocPage, PAGE_SIZE));

  LIST_INIT(&allocations);

  /*
    Allocate the first page for storing allocation metadata
  */

  setMemoryType((void *) fNextAllocPage, SIZE_1M, kernelData);

  fNextAllocPage += SIZE_1M;

  LIST_INSERT_HEAD(&allocations, (Allocation *) fNextAllocPage, link);

  hashTableInit(&addressIndex.table, ALLOC_ADDRESS_INDEX_NUM_BINS);
}
