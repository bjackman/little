#include <scheduler.h>
#include <context.h>
#include <memoryMap.h>
#include <memoryType.h>
#include <alloc.h>
#include <linkedList.h>
#include <tick.h>

#define PRINT_THREAD_LIST(l) do {                            \
    LIST_FOREACH_UNSAFE(&l, Thread, thread, link) { \
      DEBUG_SCHED("%d ", thread->threadId); \
    } \
    DEBUG_SCHED("\n"); \
  } while (0)    \


#define DEBUG_SCHED(f, ...) DEBUG(DEBUG_FLAG_SCHEDULER, "sched/%s: " f, \
                                  __func__, __VA_ARGS__)

typedef struct _Thread {
  Uintn                      threadId;
  UintPtr                    stackBase;
  ArmContext                 savedContext;
  LIST_ENTRY(struct _Thread) link;
} Thread;

LIST_HEAD(ThreadHead, Thread);

static ThreadHead fReadyQueue;

static Uintn fNumReadyThreads;

static Uintn fNextThreadId = 0;

static Thread *fCurrentThread = NULL;

#define USER_STACK_SIZE (16 * PAGE_SIZE)

void schedulerInit() {
  fNumReadyThreads = 0;
  fNextThreadId = 0;
  fCurrentThread = NULL;
  LIST_INIT(&fReadyQueue);
}

/*
  Add a thread to the scheduler.
  The argument is the function in which the thread should start.
*/
StatusCode addUserThread(void (*threadMain)(), void *entryArg) {
  Uintn threadId = fNextThreadId++;

  Thread *newThread = kalloc(sizeof(Thread));
  if (newThread == NULL) {
    printk("Couldn't allocate data structure for new thread %d\n", threadId);
    return OUT_OF_RESOURCES;
  }

  void *newStackBottom = allocatePages(USER_STACK_SIZE, userData);
  if (newStackBottom == NULL) {
    printk("Couldn't allocate a stack for new thread %d\n", threadId);
    return OUT_OF_RESOURCES;
  }

  DEBUG_SCHED("Creating thread with id %d\n", threadId);

  fNumReadyThreads++;

  // Note: stack is *descending* (that's why we add USER_STACK_SIZE)
  newThread->stackBase = (UintPtr) newStackBottom + USER_STACK_SIZE;

  newThread->threadId = threadId;
  newThread->savedContext.cpsr = DEFAULT_USER_CPSR;
  newThread->savedContext.pc = (UintPtr) threadMain;
  newThread->savedContext.lr = (UintPtr) NULL;
  newThread->savedContext.sp = newThread->stackBase;

  // Tell the thread its ID
  newThread->savedContext.r0 = threadId;
  // Pass the user's argument to the thread's entry point
  newThread->savedContext.r1 = (UintPtr) entryArg;

  LIST_INSERT_HEAD(&fReadyQueue, newThread, link);

  // If this is the first thread (added at boot time), set it as the current
  // thread.
  if (fCurrentThread == NULL) {
    fCurrentThread = newThread;
  }

  return SUCCESS;
}

StatusCode exitCurrentThread() {
  Thread *exitedThread = fCurrentThread;

  DEBUG_SCHED("Thread %d exiting\n", exitedThread->threadId);

  LIST_REMOVE(exitedThread, link);
  fNumReadyThreads--;

  // TODO: here we depend on schedule():
  // - ignores its argument
  // - always switches
  schedule(0);

  kfree((void *) exitedThread->stackBase);
  kfree(exitedThread);

  return SUCCESS;
}

void unblockThread(void *_thread) {
  Thread *thread = (Thread *) _thread;

  DEBUG_SCHED("Thread %d unblocked\n", thread->threadId);

  // TODO we're just arbitrarily sticking it at the front of the
  // list. When the scheduler is a bit more advanced we'll want to
  // think about where unblocked threads go.

  LIST_INSERT_HEAD(&fReadyQueue, thread, link);
  fNumReadyThreads++;
}


void *blockCurrentThread() {
  // Now we're going to be rather lawless: we're going to remove the
  // thread from the ready queue and forget about it, leaving it
  // floating around the timer event system. When the event is
  // triggered, the thread structure will be passed as the context
  // argument to unblockThread, so we can put it back in
  // the ready queue then.

  // TODO we're also going to assume that schedule() will move onto a
  // different thread. In the future we should factor out the
  // finding-the-next-thread logic into a separate function.

  Thread *blockedThread = fCurrentThread;

  DEBUG_SCHED("Thread %d blocked\n", blockedThread->threadId);

  LIST_REMOVE(fCurrentThread, link);

  fNumReadyThreads--;

  schedule(0);

  return blockedThread;
}

StatusCode waitCurrentThread(Uint64 waitTimeMs) {
  ASSERT(waitTimeMs > 0);

  Uint64 triggerTime = getTickCountMs() + waitTimeMs;

  DEBUG_SCHED("Thread %d waiting %dms\n", fCurrentThread->threadId,
              (Uintn) waitTimeMs);

  addTimerEvent(triggerTime, unblockThread,
                (void *) fCurrentThread);

  blockCurrentThread();

  return SUCCESS;
}

/*
  Get the context for thread 0.
  This is to be used at boot.
  TODO rename
*/
ArmContext *getInitContext() {
  // If this function is called without first creating a thread via
  // addUserThread, something has gone wrong.
  ASSERT(fNumReadyThreads > 0);
  ASSERT(LIST_FIRST(&fReadyQueue) != NULL);

  return &LIST_FIRST(&fReadyQueue)->savedContext;
}

void schedule(Uintn msSinceLastSchedule) {
  // Decide the next thread to run. If we've got to the end of the
  // list, or we were idling (fCurrentThread == NULL), go back to the
  // beginning of the list.
  if (fCurrentThread == NULL ||
      LIST_NEXT(fCurrentThread, link) == NULL) {
    fCurrentThread = LIST_FIRST(&fReadyQueue);
  } else {
    fCurrentThread = LIST_NEXT(fCurrentThread, link);
  }

  if (fCurrentThread == NULL) {
    // Setting gCurrentContextBuffer to NULL will make us idle at the
    // end of the syscall/interrupt.
    setCurrentContextBuffer(NULL);
  } else {
    setCurrentContextBuffer(&fCurrentThread->savedContext);
  }
}

/*
  The current thread caused an error. Instead of rescheduling it, we'll call the
  global signal handler in that thread.
*/
void deferSignalHandlerCall(SignalHandler handler, Uint32 argument) {
  // TODO check that the handler is within the current context's code?

  ArmContext *context = &fCurrentThread->savedContext;

  context->pc = (UintPtr) handler;
  // The thread is considered to be in error, so we just clobber its stack.
  context->sp = fCurrentThread->stackBase;
  context->r0 = argument;

  // Tell the handler which thread it is
  context->r1 = fCurrentThread->threadId;
}
