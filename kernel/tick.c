#include <alloc.h>
#include <linkedList.h>
#include <scheduler.h>
#include <tick.h>

#define DEBUG_TIMEREVENTS(f, ...) DEBUG(DEBUG_FLAG_TIMEREVENTS, "timer/%s: " f,\
                                        __func__, __VA_ARGS__)

// UINT64_MAX milliseconds will overflow in a little under 6 years.
// TODO Find a way to detect and count overflows...
//    .. then find a way to test it.
static Uint64 fMillisecondCounter;

// TODO the event stuff shouldn't be in here, it should be in a file called event.c!

typedef struct _TimerEvent {
  Uint64                         triggerTime;
  TimerEventCallback             callback;
  void                          *context;
  LIST_ENTRY(struct _TimerEvent) link;
} TimerEvent;

LIST_HEAD(TimerEventHead, TimerEvent);

static TimerEventHead fTimerEvents;

void timerEventInit() {
  LIST_INIT(&fTimerEvents);
  fMillisecondCounter = 0;
}

void onTimerTick(Uintn msSinceLastTick) {
  fMillisecondCounter += msSinceLastTick;

  // Callback events whose trigger time has passed
  TimerEvent *event = LIST_FIRST(&fTimerEvents);
  while (event != NULL && event->triggerTime < fMillisecondCounter) {
    TimerEvent *nextEvent = LIST_NEXT(event, link);

    DEBUG_TIMEREVENTS("Triggering event for %d at %d\n",
                      (Uintn) event->triggerTime, (Uintn) fMillisecondCounter);

    LIST_REMOVE(event, link);
    event->callback(event->context);
    kfree(event);

    event = nextEvent;
  }

  schedule(msSinceLastTick);
}

Uint64 getTickCountMs() {
  return fMillisecondCounter;
}

/*
  Add an item to the timer event queue. When the kernel millisecond
  counter exceeds triggerTime, call calllback, passing context as an
  argument.
*/
StatusCode addTimerEvent(Uint64 triggerTime, TimerEventCallback callback,
                         void *context) {
  ASSERT(triggerTime > fMillisecondCounter);
  ASSERT(callback != NULL);

  DEBUG_TIMEREVENTS("Timer event for %d added at %d (in %dms)\n",
                    (Uintn) triggerTime, (Uintn) fMillisecondCounter,
                    (Uintn) triggerTime - (Uintn) fMillisecondCounter);

  // Create the new event
  TimerEvent *newEvent = kalloc(sizeof(TimerEvent));
  if (newEvent == NULL) {
    return OUT_OF_RESOURCES;
  }

  newEvent->triggerTime = triggerTime;
  newEvent->callback = callback;
  newEvent->context = context;

  // The list of timer events will be sorted by trigger time.
  // Find the list item that will follow the new event.
  TimerEvent *eventAfter = LIST_FIRST(&fTimerEvents);
  while (eventAfter != NULL && eventAfter->triggerTime < triggerTime) {
    eventAfter = LIST_NEXT(eventAfter, link);
  }

  if (eventAfter == NULL) {
    LIST_INSERT_HEAD(&fTimerEvents, newEvent, link);
  } else {
    LIST_INSERT_BEFORE(eventAfter, newEvent, link);
  }

  return SUCCESS;
}
