#include <uart.h>
#include <boot.h>
#include <pageTable.h>
#include <timer.h>
#include <tick.h>
#include <cpuInterrupts.h>
#include <alloc.h>
#include <scheduler.h>

/*
  The first C code called after reset.
  The stack should be set up but everything else is uninitialised.
  To get C to empty the stack, instead of explicitly calling goToUsr from here,
  we have the caller set the LR to point at goToUsr, then "return" to it.
*/
void kernelCInit() {
  uartInit();
  printk("Hello World!\n");
  void *oldVectors = setVectorBaseAddress(&exceptionVectors);
  ASSERT(ALIGNED_ON(&exceptionVectors, 16));
  printk("VBAR was %p, now %p\n", oldVectors, (void *) &exceptionVectors);
  setupPageMapping();
  printk("Enabling MMU\n");
  setTranslationTableBase(gMasterTranslationTable);
  enableMmu();
  printk("Initialising timer hardware\n");
  timerInit(onTimerTick);
  printk("Initialising timer event system\n");
  timerEventInit();
  printk("Initialising memory allocator\n");
  initAllocator();
  printk("initialising scheduler\n");
  schedulerInit();
  StatusCode status = addUserThread(usrEntry, 0);
  ASSERT(status == SUCCESS);
  printk("Going to user mode\n");
  setCurrentContextBuffer(getInitContext());
}
