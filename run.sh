#!/bin/bash
set -eu

if [ -f Makefile.local ]; then
  source Makefile.local
fi

function cleanup {
  kill $qemu_pid
}

# assume Makefile.local is also a valid bash script
source Makefile.local

# Oh, bash. I can't (be bothered to) work out how to find out if a variable is
# unset when -u is set. So just disable it for now.
set +u
# Set default variable values
if [ -z ${BUILD_TYPE:""} ]; then
  BUILD_TYPE=DEBUG
fi
if [ -z ${SERIAL_BAUD:""} ]; then
  SERIAL_BAUD=115200
fi
if [ -z ${SERIAL_FILE:""} ]; then
  SERIAL_FILE=/dev/ttyUSB1
fi
set -u

qemu_cmd="$QEMU -M beaglexm -drive if=sd,cache=writeback,file=Build/$BUILD_TYPE/beagle-sd.img -nographic -serial mon:stdio"
gdb_cmd="${CROSS_COMPILE}gdb -q Build/$BUILD_TYPE/little.elf -x scripts/gdb/common.gdb"
start_openocd() {
  # I've never bothered to work out why, but openocd only seems to use my config
  # files if I changed directory. Specifying them by absolute path seems to make
  # no difference :/
  # By the way, this is only known to work with openocd v0.7.0
  pushd openocd/tcl
  log_file=${DIRSTACK[1]}/openocd.log
  echo "Starting OpenOCD. Output redirected to $log_file"
  openocd -f interface/flyswatter2.cfg -f board/ti_beagleboard_xm.cfg > $log_file 2>&1 &
  popd
}
serial_cmd="picocom --imap lfcrlf --baud $SERIAL_BAUD $SERIAL_FILE"

# Start Tmux with two panes. The two arguments to this function will be the
# commands run in each tmux pane.
do_tmux() {
  tmux start-server
  # Start a tmux session with the first command open
  tmux new-session -d -s little "$1"
  # Start the second command in a new pane
  tmux split-window -h -t little: "$2"
  # Enable scrolling with the mouse
  tmux setw -g mode-mouse on
  # C-b k will exit
  tmux bind-key k send-keys -t "little:0.1" "q" Enter "y" Enter '\;' kill-session
  # go into tmux
  tmux attach-session -t little
}

case ${1-} in
server)
  # Just (re)start the openocd server
  pkill openocd || true
  start_openocd
  ;;
hardware)
  # Run on hardware
  if [[ $BUILD_TYPE != "DEBUG" ]]; then
    echo "Warning, ignoring \$BUILD_TYPE ($BUILD_TYPE), using DEBUG" > /dev/stderr
  fi
  # Don't re-run openocd over-and over, it doesn't react well when it gets run
  # twice without a board reset in between.
  pgrep openocd > /dev/null || start_openocd
  # Start tmux with the serial terminal and GDB
  do_tmux "$serial_cmd | tee lastrun.log" "$gdb_cmd -x scripts/gdb/hardware_connect.gdb"
  ;;
nogdb)
  # Just run an emulator, don't bother debugging it
  $qemu_cmd
  ;;
serial)
  # Just do hardware serial
  $serial_cmd
  ;;
*)
  # Default mode: run an emulator and connect to it with gdb
  do_tmux "$qemu_cmd -s -S" "$gdb_cmd -x scripts/gdb/qemu_connect.gdb"
  ;;
esac
