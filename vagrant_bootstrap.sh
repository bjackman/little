# To be run from within Vagrant as the provisioning stage
# Remember this script will be run as root (So ~ is /root)

# Install packages
apt-get update
TOOLS="gcc-arm-none-eabi gdb-arm-none-eabi"
# Stuff required to build QEMU
QEMU_STUFF="pkg-config zlib1g-dev libglib2.0 libpixman-1-dev libfdt-dev"
apt-get install -y $TOOLS $QEMU_STUFF

# Build and install qemu
qemu_dir=/vagrant/qemu-build-cache
mkdir -p $qemu_dir
pushd $qemu_dir
/vagrant/qemu-linaro/configure --target-list=arm-softmmu
make
make install
popd

# To allow us to mess with loop devices for creating FS images
usermod -a -G disk vagrant

# Allow GDB to source .gdbinit
echo "add-auto-load-safe-path /vagrant/.gdbinit" >> ~vagrant/.gdbinit
echo 'export PS1="\[\e[00;37m\][\[\e[0m\]\[\e[00;36m\]\u\[\e[0m\]\[\e[00;37m\]:\[\e[0m\]\[\e[00;36m\]\w\[\e[0m\]\[\e[00;37m\]]\[\e[0m\]\[\e[01;36m\] \\$\[\e[0m\] "' >> ~vagrant/.bashrc
