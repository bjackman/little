#include <print.h>
#include <svc.h>
#include <syscalls.h>

// Functions for printing from user mode.

// Note the exclusion of printf, which is produced separately during the build
// process

unsigned int strLen(const char *str) {
  int i = 0;
  while (str[i] != '\0') i++;
  return i;
}

int puts(const char *str) {
  return armSvc(SYS_write, 1, str, strLen(str));
}
