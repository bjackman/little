#include <svc.h>
#include <syscalls.h>
#include <print.h>

void usrEntry() {
  printf("Waiting for character...");

  armSvc(SYS_readChar, 0);

  printf("Spinning\n");
  while(1);
}
