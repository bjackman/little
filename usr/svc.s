/* Easy way to do SVCs from C code */
.global armSvc
armSvc:
  push {lr}
  svc 1
  pop  {pc}
