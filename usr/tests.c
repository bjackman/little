#include <syscalls.h>
#include <svc.h>
#include <errors.h>
#include <print.h>

#define ARRAY_LENGTH(a) (sizeof(a) / sizeof(a[0]))

// The size we use for statically-allocated test description strings
#define DESCRIPTION_LENGTH 256

// Build a description string and set it as the description for a TestInfo
#define SET_TESTINFO_DESCRIPTION(t, ...) \
  static char description[DESCRIPTION_LENGTH]; \
  snPrintf(description, DESCRIPTION_LENGTH, __VA_ARGS__); \
  t.description = description;

// Some global state to hold information about the test currently being run.
// If shouldAbort is true, abortType holds the type of abort expected.
typedef struct {
  Boolean shouldAbort;
  SignalType abortType;
  Char8 *description;
} TestInfo;
static TestInfo *fCurrentTest = NULL;

static void passCurrentTest() {
  puts(fCurrentTest->description);
  puts(":\tPassed\n");
  fCurrentTest = NULL;
}
/*
  Report that the current test failed. Optionally add an additional message
  describing the failure

  TODO it would be nice if the message was a format string and this
  function was variadic.
*/
static void failCurrentTest(Char8 *message) {
  if (fCurrentTest == NULL) {
    puts("Failed null test. Probably a duplicate failure\n");
    return;
  }
  if (fCurrentTest->description != NULL) {
    puts(fCurrentTest->description);
  } else {
    puts("<missing test description>");
  }

  if (message == NULL) {
    puts(":\tFAILED\n");
  } else {
    printf(":\t FAILED (%s)\n", message);
  }
  fCurrentTest = NULL;
}

static void testAddressDataAborts(Uint32 address) {
  volatile int *badPointer = (int *) address;

  // Create a description of the test.
  // Needs to be static because aborts clobber the stack.
  static TestInfo t = {
    .shouldAbort = TRUE,
    .abortType = dataAbortSignal,
  };

  SET_TESTINFO_DESCRIPTION(t, "Reading address 0x%x should abort", address);

  fCurrentTest = &t;
  if (*badPointer) puts("");

  // If we got here, the test failed.
  failCurrentTest("Accessed successfully");
}

static void testAddressPrefetchAborts(Uint32 address) {
  // Cast `address` to a function pointer
  void (*badFuncPtr)() = (void (*)()) address;

  // Create test description
  // Needs to be static because aborts clobber the stack.
  static TestInfo t = {
    .shouldAbort = TRUE,
    .abortType = prefetchAbortSignal,
  };

  SET_TESTINFO_DESCRIPTION(t,
    "Attempting to execute address 0x%x should abort", address);

  fCurrentTest = &t;

  badFuncPtr();

  // If we got here, the test failed.
  // (Failing the test is actually more likely to just crash the system, though)
  failCurrentTest("Executed and returned successfully");
}

static void testGettingTickCount() {
  Uint64 tickCount1;
  Uint64 tickCount2;

  static TestInfo t = {
    .shouldAbort = FALSE,
    .description = "successive calls to SYS_getTickCountMs should produce "
                   "increasing numbers"
  };
  fCurrentTest = &t;

  tickCount1 = armSvc(SYS_getTickCountMs);
  for (Uintn i = 0; i < 100000; i++);
  tickCount2 = armSvc(SYS_getTickCountMs);

  if (tickCount1 > tickCount2) {
    failCurrentTest("Tick Count decreased.");
  } else {
    printf("\tTick counts: %d, %d (difference = %d)\n",
          (Uintn) tickCount1, (Uintn) tickCount2,
          (Uintn) (tickCount2 - tickCount1));
    passCurrentTest();
  }
}

static void testInvalidSyscallFails(Uint32 syscallNum) {
  static TestInfo t = {
    .shouldAbort = TRUE,
    .abortType = invalidSyscallSignal
  };
  SET_TESTINFO_DESCRIPTION(t, "Invalid syscall %d should fail", syscallNum);
  fCurrentTest = &t;

  armSvc(syscallNum);

  failCurrentTest("The syscall returned.");
}

#define NUM_TESTTHREAD_THREADS 15

static Boolean threadRan[NUM_TESTTHREAD_THREADS] = { 0 };

static void secondaryThreadFunction(Uintn threadId) {
  printf("Hello from thread %d\n", threadId);

  if (threadRan[threadId - 1]) {
    char message[256];
    snPrintf(message, 256, "It looks like thread %d ran twice", threadId);
    failCurrentTest(message);
  }

  threadRan[threadId - 1] = TRUE;

  StatusCode status = armSvc(SYS_exit);

  // Shouldn't get here.
  printf("WARNING: SYS_exit didn't work in thread %d, returned %d\n", threadId, (Uintn) status);
  while(1);
}

static void testMultipleThreadsRun() {
  static TestInfo t = {
    .shouldAbort = FALSE,
    .description = "We can run other threads"
  };
  fCurrentTest = &t;

  // Spawn a load of threads
  for (Uintn i = 0; i < NUM_TESTTHREAD_THREADS; i++) {
    StatusCode status = armSvc(SYS_createThread, secondaryThreadFunction);
    if (status != SUCCESS) {
      failCurrentTest("SYS_createThread failed");
      return;
    }
  }

  printf("Spawned %d threads. Waiting\n", NUM_TESTTHREAD_THREADS);

  // Wait a while
  Uint64 ticks = armSvc(SYS_getTickCountMs);
  while (armSvc(SYS_getTickCountMs) < (ticks + 3000));

  // Check all the threads ran
  for (Uintn i = 0; i < NUM_TESTTHREAD_THREADS; i++) {
    if (!threadRan[i]) {
      failCurrentTest("All threads didn't get run in 3000ms");
      return;
    }
  }
  passCurrentTest();
}

// Test that we can't access dynamically allocated kernel memory.
// This test assumes that they first memory allocated is owned by the kernel.
void testCantAccessKernelMemory() {
  // This will break in the imagined future, but right now we're statically
  // linked against the kernel, so we can simply access its symbols.

  // This symbol is defined by the linker and points to the first page that the
  // kernel's allocator can allocate
  extern int gAllocatableMemoryStart;

  printf("Trying to access kernel dynamic memory at 0x%p\n",
         &gAllocatableMemoryStart);
  testAddressDataAborts((UintPtr) &gAllocatableMemoryStart);
}

#define NUM_TESTWAIT_THREADS 10

static Boolean fTestWaitFailed = FALSE;

// TODO A nicer way to do this would be to add a syscall to find out how many
// threads exist in the process. We could then just wait until that number is 1.
static Boolean fTestWaitThreadsDone[NUM_TESTWAIT_THREADS];
static Uintn fTestWaitHelperIdOffset;

// This function will be called as the entry point for several threads
void testWaitHelper(Uintn threadId) {
  // Pick a messy wait time (less than 5 seconds)
  // NOTE: It's very important that this is a Uint64! Since armSVC
  // doesn't have any type info attached, a smaller type won't get
  // implicitly cast, so the AEABI won't get followed and the syscall
  // will get garbage!
  Uint64 waitTimeMs = (3671 * threadId) % 5000;

  Uint32 time1 = armSvc(SYS_getTickCountMs);

  StatusCode status = armSvc(SYS_wait, waitTimeMs);
  if (status != SUCCESS) {
    printf("ERROR: SYS_wait returned %d in thread %d\n", status, threadId);
    fTestWaitFailed = TRUE;
  }

  Uint32 time2 = armSvc(SYS_getTickCountMs);
  if (time2 - time1 < waitTimeMs) {
    printf("ERROR: Thread %d didn't wait long enough\n", threadId);
    fTestWaitFailed = TRUE;
  }

  fTestWaitThreadsDone[threadId - fTestWaitHelperIdOffset] = TRUE;

  status = armSvc(SYS_exit);

  // Shouldn't get here.
  printf("WARNING: SYS_exit didn't work in thread %d, returned %d\n", threadId, (Uintn) status);
  while(TRUE);
}

void testWait() {
  static TestInfo test =  {
    .shouldAbort = FALSE,
    .description = "SYS_wait works in multiple threads"
  };
  fCurrentTest = &test;

  // Guess the thread ID of the first thread we'll create. This simply
  // assumes that testCreateThread runs before testWait.
  // TODO get this in some better way that doesn't depend on test ordering.
  // Probably SYS_createThread should return the ID of the thread that
  // was created.
  fTestWaitHelperIdOffset = NUM_TESTTHREAD_THREADS + 1;

  // Initialise markers to test when threads are done
  for (Uintn i = 0; i < NUM_TESTWAIT_THREADS; i++) {
    fTestWaitThreadsDone[i] = FALSE;
  }

  // Spawn some threads that will exercise SYS_wait
  for (Uintn i = 0; i < NUM_TESTWAIT_THREADS; i++) {
    StatusCode status = armSvc(SYS_createThread, testWaitHelper);
    if (status != SUCCESS) {
      failCurrentTest("Couldn't spawn thread\n");
      return;
    }
  }

  Uint64 startTimeMs = armSvc(SYS_getTickCountMs);

  // Wait until all threads seem to be finished, or we time out at 10 seconds.
  Boolean allThreadsFinished = FALSE;
  while (!allThreadsFinished &&
         armSvc(SYS_getTickCountMs) < startTimeMs + 10000) {
    allThreadsFinished = TRUE;
    for (Uintn i = 0; i < NUM_TESTWAIT_THREADS; i++) {
      if (!fTestWaitThreadsDone[i]) {
        allThreadsFinished = FALSE;
      }
    }
    if (!allThreadsFinished) {
      printf(".");
    }
  }
  printf(".\n");

  if (!allThreadsFinished) {
    failCurrentTest("Looks like some threads got stuck\n");
  } else if (fTestWaitFailed) {
    failCurrentTest(NULL);
  } else {
    passCurrentTest();
  }

  // Now let's see if we can halt when there is only one thread.

  static TestInfo test2 =  {
    .shouldAbort = FALSE,
    .description = "SYS_wait works when there is only one thread"
  };
  fCurrentTest = &test2;

  startTimeMs = armSvc(SYS_getTickCountMs);

  StatusCode status = armSvc(SYS_wait, (Uint64) 1000);
  if (status != SUCCESS) {
    printf("Warning: SYS_wait returned %d\n", status);
    failCurrentTest(NULL);
  } else if (armSvc(SYS_getTickCountMs) < (startTimeMs + 1000)) {
    failCurrentTest("Didn't wait long enough");
  } else {
    passCurrentTest();
  }
}

static void testExitHelper(Uintn threadId) {
  TestInfo t = {.shouldAbort = FALSE,
    .description = "SYS_exit works"
  };
  fCurrentTest = &t;

  StatusCode status = armSvc(SYS_exit);
  printf("WARNING: SYS_exit returned %d\n", status);

  failCurrentTest(NULL);

  while(TRUE);
}

static void testExit() {
  StatusCode status = armSvc(SYS_createThread, testExitHelper);
  if (status != SUCCESS) {
    printf("ERROR: testExit couldn't create a thread\n");
    return;
  }

  // Wait a while.
  // We don't really mind if this doesn't work:
  // - If testExitHelper hasn't run at all yet passCurrentTest will yell
  // - If testExitHelper has run and wants to fail but hasn't yet,
  //   failCurrentTest will yell.
  armSvc(SYS_wait, (Uint64) 2000);

  // If the other thread hasn't failed the test, it passed.
  if (fCurrentTest != NULL) {
    passCurrentTest();
  }
}

static void testReadCharFails(Uint32 fd) {
  TestInfo t = {
    .shouldAbort = FALSE,
  };
  SET_TESTINFO_DESCRIPTION(t, "SYS_readChar fails for descriptor %d", fd);
  fCurrentTest = &t;

  Intn result = armSvc(SYS_readChar, (Uintn) fd);

  if (result == -1) {
    passCurrentTest();
  } else {
    printf("ERROR: readChar returned %d\n", result);
    failCurrentTest(NULL);
  }
}

static void testReadChar() {
  static TestInfo t = {
    .shouldAbort = FALSE,
    .description = "SYS_readChar seems to work"
  };
  fCurrentTest = &t;

  printf("Waiting for input, will echo...");

  for (Uintn i = 0; i < 5; i++) {
    Intn result = armSvc(SYS_readChar, 0);
    if (result < 0) {
      failCurrentTest("SYS_readChar failed");
      break;
    } else {
      printf("%c", (Char8) result);
    }
  }
  printf("\n");

  if (fCurrentTest != NULL) {
    passCurrentTest();
  }
}


static void doTests() {
  // This function will cause aborts. We use `position` to remember how far
  // through the tests we got before the last abort.
  static Uintn position = 0;

  char stackMemory[] = "This is some data on the stack";

  // An array of pairs of test unary functions and arguments.
  // Each function will be called with its corresponding argument in turn.
  struct Test {
    void (*func)(Uint32 arg);
    Uint32 arg;
  } tests[] = {
    { testAddressDataAborts, 0x00000000 },
    { testAddressDataAborts, 0x40000000 },
    { testAddressDataAborts, 0x80000000 },
    { testAddressDataAborts, 0xa0000000 },
    { testAddressPrefetchAborts, 0x00000000 },
    { testAddressPrefetchAborts, 0x40000000 },
    { testAddressPrefetchAborts, 0x80000000 },
    { testAddressPrefetchAborts, 0xa0000000 },
    { testAddressPrefetchAborts, (Uint32) stackMemory },
    { testGettingTickCount, 0 },
    { testInvalidSyscallFails, numSyscalls },
    { testInvalidSyscallFails, UINT32_MAX },
    { testReadCharFails, 3 },
    { testReadCharFails, -1 },
    { testCantAccessKernelMemory, 0 },
    { testMultipleThreadsRun, 0 },
    { testWait, 0 },
    { testExit, 0 },
    { testReadChar, 0 }
  };

  // Run each test in turn.
  // Note that we must increment `position` *before* calling the test function,
  // as an abort may take place in the test function.
  while (position < ARRAY_LENGTH(tests)) {
    struct Test *t;
    t = tests + position;

    position++;
    t->func(t->arg);
  }

  puts("Finished tests.");
  while(1);
}

// A signal handler that checks whether the currently executing test is expected
// to cause an abort. If it is, is records that the test was successful.
// Otherwise it reports that the test has caused an error.
static void signalHandler(SignalType receivedSignalType, Uintn threadId) {
  if (fCurrentTest == NULL) {
    // TODO describe the signal
    printf("ERROR: received signal %d while not executing a test\n",
           receivedSignalType);
    while(1);
  }

  if (fCurrentTest->shouldAbort) {
    if (receivedSignalType == fCurrentTest->abortType) {
      passCurrentTest();
    } else {
      failCurrentTest("Caused the wrong type of abort");
      fCurrentTest = NULL;
    }
  } else {
    failCurrentTest("Caused an abort");
  }

  doTests();
}


// To be run on initial branch to usr mode
void usrEntry() {
  armSvc(SYS_handleSignal, signalHandler);

  puts("hello from user mode\n");

  doTests();
}
