#ifndef __HELPERS_H__
#define __HELPERS_H__

Uintn readLine(Char8 *buf, Uintn size);

StatusCode parseUintn(Char8 *s, Uintn *out);

void wait(Uint64 waitTimeMs);
void exit();

#endif
