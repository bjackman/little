#ifndef __PRINT_H__
#define __PRINT_H__

int puts(const char *str);

unsigned int strLen(const char *str);

void printf(Char8 *format, ...) __attribute__((format(printf, 1, 2)));

void snPrintf(Char8 *output, Uintn length, Char8 *format, ...);

#endif
