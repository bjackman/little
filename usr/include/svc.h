#ifndef __SVC_H__ 
#define __SVC_H__

Uint64 armSvc(unsigned int svcNum, ...);

#endif
