#include <syscalls.h>
#include <svc.h>
#include <print.h>

void thread1() {
  while (TRUE) {
    printf("A\n");
  }
}

void thread2() {
  while (TRUE) {
    printf("B\n");
  }
}

void usrEntry() {
  armSvc(SYS_createThread, thread1, 0);
  armSvc(SYS_createThread, thread2, 0);
  while(TRUE);
}
