#include <svc.h>
#include <syscalls.h>
#include <print.h>
#include <helpers.h>
#include <errors.h>

void usrEntry();

void signalHandler(SignalType receivedSignalType, Uintn threadId) {
  switch (receivedSignalType) {
    case prefetchAbortSignal:
      printf("Prefetch abort\n");
      break;
    case dataAbortSignal:
      printf("Data abort\n");
      break;
    default:
      printf("Other signal (%d)\n", receivedSignalType);
      break;
  }
  usrEntry();
}

void testFunction() {
  printf("You called the test function!\n");
}

static Uint32 testVariable = 0x123;

#define READLINE_BUF_SIZE 256
void usrEntry() {
  Char8 buf[READLINE_BUF_SIZE];

  printf("Test function at 0x%p\n", testFunction);
  printf("test variable at 0x%p\n", &testVariable);

  armSvc(SYS_handleSignal, signalHandler);

  while (TRUE) {
    StatusCode status = INVALID_PARAMETER;
    UintPtr addr;
    while (status != SUCCESS) {
      printf("Enter an address: ");
      readLine(buf, READLINE_BUF_SIZE);
      status = parseUintn(buf, &addr);
    }

    printf("Read (r) or execute (e)?");
    Char8 c = '\0';
    while (c != 'r' && c != 'e') {
      c = armSvc(SYS_readChar, 0);
    }
    printf("\n");

    if (c == 'r') {
      printf("*0x%x == 0x%x\n", addr, *(Uintn *) addr);
    } else {
      ((void (*)()) addr)();
    }
  }
}
