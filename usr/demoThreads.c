#include <svc.h>
#include <syscalls.h>
#include <print.h>
#include <helpers.h>

void threadEntry(Uintn threadId, void *arg) {
  UintPtr waitTimeMs = (UintPtr) arg;

  armSvc(SYS_wait, (Uint64) waitTimeMs);

  printf("\nThread %d finished waiting %dms\n", threadId, waitTimeMs);

  armSvc(SYS_exit);
}

#define READLINE_BUF_SIZE 256
void usrEntry() {
  Char8 buf[READLINE_BUF_SIZE];

  while (TRUE) {
    printf("Enter a time in milliseconds: ");
    readLine(buf, READLINE_BUF_SIZE);

    Uintn n;
    StatusCode status = parseUintn(buf, &n);
    if (status == SUCCESS) {
      armSvc(SYS_createThread, threadEntry, (void *) n);
    } else {
      printf("Failed to parse number.\n");
    }
  }
}
