#include <syscalls.h>
#include <svc.h>

void usrEntry() {
  Char8 helloWorldString[] = "Hello World!";

  armSvc(SYS_write, 0, helloWorldString, sizeof(helloWorldString));

  while(1) {
    ;
  }
}
