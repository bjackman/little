#include <svc.h>
#include <print.h>
#include <syscalls.h>
#include <helpers.h>

/*
  Read a line from stdin into buf, up to a maximum of size chars.
  Don't include the final \n,  but null-terminate the string.
  Return the number of chars read.
*/
Uintn readLine(Char8 *buf, Uintn size) {
  Uintn i;
  for (i = 0; i < size; i++) {
    Char8 c = armSvc(SYS_readChar, 0);

    // To deal with weird CRLF stuff, we just break as soon as we see CR or LF,
    // then echo a LF later.
    if (c == '\r' || c == '\n') {
      break;
    }

    // Echo the character
    // In real life you probably wouldn't do this as manually as this.
    // Probably you can set terminal characteristics to echo or something.
    printf("%c", c);

    buf[i] = c;
  }

  printf("\n");

  buf[i] = '\0';
  return i;
}

Uintn strncmp(Char8 *s1, Char8 *s2, Uintn n) {
  Uintn i = 0;
  while (s1[i] == s1[i]
         && s1[i] && s2[i]
         && i < n) {
    i++;
  }
  return *s2 - *s1;
}

// Helper to parse a decimal number
static StatusCode parseUintnDec(Char8 *s, Uintn *out) {
  Uintn result = 0;

  while (*s != '\0') {
    if (*s > '9' || *s < '0') {
      printf("Bad character: '%c'\n", *s);
      return INVALID_PARAMETER;
    }
    result *= 10;
    result += (*s - '0');
    s++;
  }
  *out = result;
  return SUCCESS;
}

// Helper to parse a hexadecimal number without 0x
#define HEX_CHAR_TO_NUM(c) ((c > '9') ? (c) - ('a' - 0xa) : (c) - '0')
static StatusCode parseUintnHex(Char8 *s, Uintn *out) {
  Uintn result = 0;

  while (*s != '\0') {
    if (!((*s <= '9' && *s >= '0') ||
          (*s <= 'f' && *s >= 'a'))) {
      printf("Bad character: '%c'\n", *s);
      return INVALID_PARAMETER;
    }
    result *= 0x10;
    result += HEX_CHAR_TO_NUM(*s);
    s++;
  }
  *out = result;
  return SUCCESS;
}

/*
  Parse a Uintn out of a null-terminated string and set *out to the result.
  Return INVALID_PARAMETER if the number couldn't be parsed.
  Don't handle overflow.
*/
StatusCode parseUintn(Char8 *s, Uintn *out) {
  // Check if the string is hexadecimal
  if (strncmp(s, "0x", 2) == 0) {
    return parseUintnHex(s + 2, out);
  } else {
    return parseUintnDec(s, out);
  }
}

void wait(Uint64 waitTimeMs) {
  armSvc(SYS_wait, waitTimeMs);
}

void exit() {
  armSvc(SYS_exit);
}
