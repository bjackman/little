#include <stdint.h>

// Base types and values for Little

#ifndef __BASE_H__
#define __BASE_H__

typedef uint_fast32_t Uintn;
#define UINTN_MAX UINT_FAST32_MAX
typedef int_fast32_t  Intn;
#define UINTN_MAX UINT_FAST32_MAX

typedef int8_t     Int8;
typedef int16_t    Int16;
typedef int32_t    Int32;
typedef int64_t    Int64;

typedef uint8_t    Uint8;
typedef uint16_t   Uint16;
typedef uint32_t   Uint32;
typedef uint64_t   Uint64;

typedef char       Char8;

typedef Uintn      Boolean;
#define TRUE  1
#define FALSE 0

typedef uintptr_t  UintPtr;

typedef enum {
  SUCCESS = 0,
  OUT_OF_RESOURCES,
  IN_USE,
  NOT_IMPLEMENTED_YET,
  INVALID_PARAMETER
} StatusCode;

#define NULL ((void *)0)

#define SIZE_1K  0x0400
#define SIZE_4K  (SIZE_1K * 4)
#define SIZE_16K (SIZE_1K * 16)

#define SIZE_1M  (SIZE_1K * SIZE_1K)

// TODO, hopefully we can change this to 4k soon.
#define PAGE_SIZE (SIZE_1M)

#define ROUND_UP_ALWAYS(val, roundTo) ((((val) / (roundTo)) + 1) * (roundTo))

#define ROUND_UP(val, roundTo) ROUND_UP_ALWAYS((val) - 1, (roundTo))

#define NEXT_PAGE_BOUNDARY(val) ROUND_UP_ALWAYS((UintPtr) val, PAGE_SIZE)

#define ALIGNED_ON(val, alignment) ((((Uint32) (val)) & ((alignment) - 1)) == 0)

// Sorry, have to include these at the end otherwise the base types won't be
// defined yet when they're included. There is probably a better way around this
// but frankly I find the idea of looking for it deeply boring.
#include <copyMem.h>
#include <debug.h>

#endif
