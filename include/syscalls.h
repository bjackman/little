#ifndef __SYSCALLS_H__
#define __SYSCALLS_H__

enum {
  // void write(Uintn fileDescriptor, Char8 *str, Uintn length)
  SYS_write = 0,
  // Read a character from the file, blocking if none is available yet.
  // Returns -1 if fileDescriptor is invalid.
  // Intn readChar(Uintn fileDescriptor)
  SYS_readChar,
  // Handle an abort. Just calls the function provided.
  // void handleSignal(void (*handler)())
  SYS_handleSignal,
  // Get the count of millisecond ticks from the timer. Remember that the
  // resolution of the timer will be coarser than 1ms
  SYS_getTickCountMs,
  // Create a new thread, starting at the function provided.
  // threadMain will be called with the threads ID and entryArg as its arguments.
  // void createThread(void (*threadMain)(), void *entryArg)
  SYS_createThread,
  // Block for (at least) the given number of milliseconds
  // StatusCode wait(Uint64 waitTimeMs)
  SYS_wait,
  // Exit the current thread
  // StatusCode exit()
  SYS_exit,

  numSyscalls,
};

Uint32 gMaxSyscallNumber;

#pragma pack(0)
extern void (*syscallHandlers[])();
#pragma pack()

// Handler for when there's an invalid syscall
extern void invalidSyscall();

#endif
