The Mustard Kernel
========

Mustard is a shitty OS Kernel for the BeagleBoard xM. I'm writing it as a learning exercise and for my university coursework.

At time of writing this README, all code directly included in this repository is copyright Brendan Jackman, 2014-2015. Other code with different copyright and licensing is linked via Git submodules.

This code is licensed under the GNU General Public License v3. See COPYING.txt for details, or view the license on the web at `http://www.gnu.org/licenses/gpl.txt`.

Right now we have memory protection, some multithreading and UART output - that's about it. Check in `usr/demo*.c` for examples of the kernel's interface. One day we will have processes and proper I/O.

How to build and run Mustard
------------
The development environment is encapsulated by Vagrant (how trendy is that?). Just install VirtualBox and Vagrant, clone this repo, `cd` into the repo directory and run these commands:

    git submodule update
    vagrant up            # This will take maybe 30 mins the first time
    vagrant ssh
    cd /vagrant
    make
    ./run.sh

You'll be in a `tmux` session with the emulator's output in one pane and a GDB prompt in the other. The emulator will be halted. Type `continue` to let the kernel run. Marvel at the intelligence its author must have. Note with a wry smile, however, that some of the tests are failing.

`Ctrl+b` followed by `k` will kil the `tmux` session.

If you have a BeagleBoard xM, you are a wonderful person and you can run Mustard on hardware. `make` it, and `dd` beagle-sd.img onto yor Beagle's SD card. Plug it in, boot it up, bask in the rays of my achievement.


Compiling without Vagrant
------------
The following is obsoleted by the use of Vagrant, but I've left it here in case I (or anyone else) needs to work out how to build and run the system without it. It will probably only ever work on Linux and I haven't made any effort to work out the dependencies. If you are reading this in the future, the following is probably out of date.

- Install a GCC cross compiler for ARM - the target should be "arm-none-eabi". If you're lucky, it will be packaged for your host OS, but you'll probably need to [build it](http://wiki.osdev.org/GCC_Cross-Compiler). For this project you only need support for C (not C++).
  Try [this](https://launchpad.net/gcc-arm-embedded/4.7/4.7-2013-q1-update).

- Locate the standard headers that came with the cross-compiler. This directory should contain `stdarg.h` and `stdint.h`.

- Use `make` to build. The Makefile will prepend the value of `CROSS_COMPILE` to all the toolchain commands it uses. You'll also need to set SYS_INCLUDE to the directory you located in the previous step. For me, the `make` command looks like this: `SYS_INCLUDE=/usr/local/cross/arm-none-eabi/include/ CROSS_COMPILE=/usr/local/cross/bin/arm-none-eabi- make`. The top-level Makefile includes Makefile.local before it does anything, so you can set these variables there.

- Compile the version of QEMU included as a git submodule in `qemu-linaro`. Install it or add it to your path.

- Use run.sh to run on QEMU. It will start a `tmux` session with Qemu in one pane and GDB in the other. Exit it with `ctrl + b` followed by `k`. The Qemu monitor is muliplexed onto the terminal output. Focus the QEMU pane and press `C-a`, `c` to switch between them.

- If you want to debug your BBxM with a Tincantools Flyswatter2, you should be able to compile the version of OpenOCD included in the submodule, install it, and use `./run.sh hardware` to run on hardware with a JTAG connection.
