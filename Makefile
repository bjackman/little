-include Makefile.local

ifndef CROSS_COMPILE
$(warning CROSS_COMPILE not set, using system toolchain.)
endif
ifndef SYS_INCLUDE
$(warning SYS_INCLUDE not set, standard headers may not be found.)
endif

# TODO make this happen automatically..
INC = -I$(SYS_INCLUDE) \
			-Iinclude -Ikernel/include -Iusr/include -include base.h

BUILD_TYPE ?= DEBUG
DEBUG_FLAGS ?= 0x0

ifeq ($(BUILD_TYPE),DEBUG)
	DEF = -DDEBUG_BUILD -DDEBUG_FLAGS=$(DEBUG_FLAGS)
else
	DEF =
endif

BUILD = Build/$(BUILD_TYPE)

# Compile with GCC, but tell it it's assembly code. This allows us to use the
# C preprocessor in assembly.
AS = $(CROSS_COMPILE)gcc -x assembler-with-cpp
ASFLAGS = -g -mcpu=cortex-a8 -Ikernel/include $(DEF)

CC = $(CROSS_COMPILE)gcc
CFLAGS = --std=c99 -g3 -nostdlib -nostdinc -ffreestanding -Wall -Wextra -Werror \
		-Wno-unused-parameter -Wno-error=unused-function $(DEF) $(INC)

LD = $(CROSS_COMPILE)gcc
OBJCOPY = $(CROSS_COMPILE)objcopy

USERSPACE_MAIN ?= usr/tests.c

.PHONY: all compile run bssLoad
all: $(BUILD)/little.bin $(BUILD)/beagle-sd.img
compile: $(BUILD)/little.elf
run: all
	./run.sh $(RUN_ARGS)
bssLoad: $(BUILD)/little.elf
	# Set the LOAD flag in little.elf's .bss.
	# This is useful because otherwise GDB's `restore` command ignores it and
	# zero-initialised variables aren't zero-initialised.
	$(OBJCOPY) --set-section-flags .bss=alloc,load $<

beagleSources_ = interruptController.c  timer.c  uart.c
beagleSources = $(addprefix beagleDrivers/, $(beagleSources_))

kernelSources_ = cEntry.c errors.c mmio.c pageTable.c memoryUtils.c \
	syscall.c boot.s vectors.s $(beagleSources) interrupt.c \
	memoryBarriers.s cpuInterrupts.s tick.c printk.c context.s memset.c \
	armSysCtrlRegisters.s context.c scheduler.c copyMem.c alloc.c hashTable.c \
	read.c
kernelSources = $(addprefix kernel/, $(kernelSources_))

usrSources = usr/svc.s usr/print.c usr/helpers.c $(USERSPACE_MAIN)

sources = $(kernelSources) $(usrSources)

asmFiles = $(filter %.s, $(sources))
cFiles   = $(filter %.c, $(sources))

cObjs   = $(addprefix $(BUILD)/, $(cFiles:.c=.o))
$(cObjs): $(BUILD)/%.o: %.c
asmObjs = $(addprefix $(BUILD)/, $(asmFiles:.s=.obj))
$(asmObjs): $(BUILD)/%.obj: %.s

objs = $(cObjs) $(asmObjs) $(BUILD)/usr/printf.o

$(objs): | $(BUILD)/kernel/beagleDrivers $(BUILD)/usr

$(BUILD)/usr/printf.o: $(BUILD)/kernel/printk.o
	$(OBJCOPY) --redefine-sym printk=printf --redefine-sym uartPrint=puts \
		--redefine-sym snPrintk=snPrintf $< $@

$(BUILD)/little.bin: $(BUILD)/little.elf
	$(OBJCOPY) $(BUILD)/little.elf -O binary $(BUILD)/little.bin

$(BUILD)/little.elf: $(objs) link.ld $(BUILD)/CFLAGS.txt
	$(LD) -T link.ld -o $(BUILD)/little.elf $(objs) -ffreestanding -nostdlib \
	  -lgcc -Xlinker -Map=$(BUILD)/little.map

-include $(cObjs:.o=.d)

# Awkward way to force rebuild if the compile flags change
.PHONY: force
$(BUILD)/CFLAGS.txt: force
	echo '$(CFLAGS)' | cmp -s - $@ || echo '$(CFLAGS)' > $@

# C files
$(BUILD)/%.o: %.c
	$(CC) -c $(CFLAGS) -o $@ $<
	@# Auto generate rules to express header file dependencies
	$(CC) $(CFLAGS) -MM -MQ $@ $< > $(BUILD)/$*.d
# Assembly files
$(BUILD)/%.obj: %.s
	$(AS) -c $(ASFLAGS) -o $@ $<

# Create the output directory if it doesn't exist
$(BUILD)/kernel/beagleDrivers:
	mkdir -p $(BUILD)/kernel/beagleDrivers
$(BUILD)/usr:
	mkdir -p $(BUILD)/usr

.PHONY: u-boot
u-boot: u-boot/u-boot.img u-boot/MLO
u-boot/MLO u-boot/u-boot.img: u-boot/
	# Problem: The U-Boot build depends on mmap() to create image files.
	# This doesn't work in Virtualbox's shared directories. To work around that,
	# we copy the u-boot tree to /tmp and build it there.
	cp -r u-boot/ /tmp
	make -C /tmp/u-boot omap3_beagle_config
	CROSS_COMPILE=$(CROSS_COMPILE) make -C /tmp/u-boot -j4
	cp /tmp/u-boot/u-boot.img /tmp/u-boot/MLO u-boot
	rm -rf /tmp/u-boot

.DELETE_ON_ERROR:

$(BUILD)/beagle-sd.img: $(BUILD)/little.bin u-boot/u-boot.img u-boot/MLO uEnv.txt
	# Another workaround for VirtualBox shared directories (see above)
	scripts/mk_sd_img.sh ~/beagle-sd.img $(BUILD)/little.bin u-boot/u-boot.img u-boot/MLO uEnv.txt
	mv ~/beagle-sd.img $(BUILD)

clean:
	rm -r $(BUILD)/*
