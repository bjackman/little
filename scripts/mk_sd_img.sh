#!/bin/bash
set -eu
set -o pipefail

SCRIPT_NAME=$0

debug() {
  echo "$SCRIPT_NAME: $1" 1>&2
}

# Creates a disk image with a single partition, formats it with vfat, then
# copies all the provided files onto it.
# First argument is output file, remaining arguments are files to copy in.

# Set up a loop device so partitions on it can be mounted
setup_loop_device () {
  img=$1

  # Remove the '*' that marks a partition as bootable
  remove_bootable_star="sed s/\*//g"

  # Squash consecutive whitespace characters into a single tab character
  squash_whitespace="sed s/\s\+/\t/g"

  # If the file doesn't exist, fdisk just succeeds silently.
  # So check manually. Cheers, fdisk.
  if [ ! -f "$img" ]; then
    echo "$img is not a file"
    exit 1
  fi

  # Scrape out the offset of the partition
  partition_offset_in_sectors=$(fdisk -l $img | $remove_bootable_star | $squash_whitespace | grep ${img}1 | cut --fields=2)

  debug "partition starts in sector $partition_offset_in_sectors"

  # I really, really cannot be bothered to work out sector size.
  # This script is already more than boring enough. Assume 512 bytes

  partition_offset=$(( $partition_offset_in_sectors * 512 ))

  # Set up a loop device (Todo: Could probably also do --sizelimit)
  loopdev=$(losetup -f --show --offset $partition_offset $img)

  debug "Set up $loopdev to point to $img"

  echo $loopdev
}

image=$1
shift

function teardown_loopdev() {
  debug "Tearing down $loopdev"
  losetup -d $loopdev
}


# Use "seek" instead of "count" to create a sparse file
debug "Creating image $image"
dd if=/dev/zero of=$image bs=1M seek=2K count=0 2> /dev/null

# Create a single MBR FAT partition
debug "Creating MBR FAT partition in $image"
parted -s $image mklabel msdos
parted -s $image mkpart primary fat32 1 2048
parted -s $image set 1 boot on

#
# Create a filesystem on that partition:
#

loopdev=$(setup_loop_device $image)

trap teardown_loopdev EXIT


# Create the filesystem
debug "Creating filesystem on $loopdev"
mkfs.vfat -n boot -F 32 $loopdev

function cleanup() {
  debug "Cleaning up $loopdev"
  sync
  sudo umount $loopdev
  losetup -d $loopdev
  debug "Unmounting $loopdev from $mount_point"
  rmdir $mount_point
}

mount | grep $loopdev && ( debug "$loopdev already mounted. Exiting"; exit 1 )

# Mount the loop device and extract the mount point
mount_point=$(mktemp -d)
debug "Mounting $loopdev at $mount_point"
mount_point=$(mktemp -d)
sudo mount -t vfat $loopdev $mount_point

trap cleanup EXIT

debug "Copying files to $mount_point"
sudo cp -t $mount_point $*

