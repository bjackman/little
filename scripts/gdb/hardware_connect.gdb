# connect to openocd
target remote localhost:3333
define reset
  monitor reset halt
  restore Build/DEBUG/little.elf
  file Build/DEBUG/little.elf
  set $pc = 0x80200000
end
reset
