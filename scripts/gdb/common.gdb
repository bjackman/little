set history filename ~/.gdb_history
set history save

define disable_interrupts
  set $cpsr=$cpsr | (1 << 7)
end

define enable_interrupts
  set $cpsr=$cpsr & ~(1 << 7)
end
