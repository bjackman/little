#!/bin/bash

set -eu
set -o pipefail

img=$1

# Remove the '*' that marks a partition as bootable
remove_bootable_star="sed s/\*//g"

# Squash consecutive whitespace characters into a single tab character
squash_whitespace="sed s/\s\+/\t/g"

# If the file doesn't exist, fdisk just succeeds silently.
# So check manually. Cheers, fdisk.
if [ ! -f "$img" ]; then
  echo "$img is not a file"
  exit 1
fi

# Scrape out the offset of the partition
partition_offset_in_sectors=$(fdisk -l $img | $remove_bootable_star | $squash_whitespace | grep ${img}1 | cut --fields=2)

# I really, really cannot be bothered to work out sector size.
# This script is already more than boring enough. Assume 512 bytes

partition_offset=$(( $partition_offset_in_sectors * 512 ))

# Set up a loop device (Todo: Could probably also do --sizelimit)
loopdev=$(losetup -f --show --offset $partition_offset $img)

echo $loopdev
