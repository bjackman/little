#!/bin/bash
set -eu
set -o pipefail

img=$1
shift

function cleanup() {
  sync
  sudo umount $loopdev
  losetup -d $loopdev
}

# Run kpartx and extract the name of the loop device it created
loopdev=$(scripts/setup_loop_device.sh $img)

trap cleanup EXIT

mount | grep $loopdev && exit 1

# Mount the loop device and extract the mount point
# mount_point=$(mktemp -d)
mount_point=$(mktemp -d)
sudo mount -t vfat $loopdev $mount_point

cp -t $mount_point $*
