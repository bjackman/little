set -eu

home_vagrant_d=~/.vagrant.d
tmp_vagrant_d=/tmp/vagrant.d.$(whoami)

fix_link () {
  link_target="$1"
  link_name="$2"

  # Does the link exist? (Use -L to include broken symlinks)
  if [ -f "$link_name" -o -L "$link_name" ]; then
    # Check that it is a symlink
    if [ ! -L "$link_name" ]; then
      echo "$0: $link_name exists and is not a symlink. Quitting."
      exit 1
    fi
  else
    # The link doesn't exist at all. Create it
    mkdir -p "$link_target"
    ln -s "$link_target" "$link_name"
  fi

  # Check if the link's target exists
  if [ ! -r "$link_name" ]; then
    f=$(readlink "$link_name")
    echo "$link_name is a broken link. Creating $f"
    mkdir $f
  fi
}

fix_link /tmp/vagrant.d.$(whoami) ~/.vagrant.d
fix_link /tmp/vbox.$(whoami) $HOME"/VirtualBox VMs"

